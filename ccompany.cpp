#include "ccompany.h"

CCompany::CCompany(QString name, QString ticker, int numFinam, QString linkInvesting) :
    m_name(name), m_ticker(ticker), m_numFinam(numFinam), m_linkInvesting(linkInvesting), m_tickerPreff("Empty"), m_type(0)
{
}

void CCompany::analyzeDayData(DD_struct ddOpts, WC_struct &data)
{
//    QMapIterator<int, QMap<int, CFinData*> > i(m_dataComp);
//    while (i.hasNext()) {
//        i.next();
//        QMapIterator<int, CFinData*> j(i.value());
//        while (j.hasNext()) {
//            j.next();
//            j.value()->analyze();
//        }
//    }

    QList<double> procentChange;
    for(int i = 0; i < listPrice.size(); i++) {
        if(i+1 < listPrice.size()) {
            double tmp = listPrice[i+1].close - listPrice[i].close;
            double tmpProcent = tmp/listPrice[i].close*100;
            procentChange.append(tmpProcent);
        }
    }

    int countUp = 0, countDown = 0;

    for(int i = procentChange.size()-1; i >= 0; i--) {
        if(procentChange[i] > 0) {
            countUp++;
         //   percentDown--;
        }
        else if(procentChange[i] < 0) {
            countDown++;
           // percentUp--;
        }
    }

    double sumPercents = 0;
    for(int i = 0; i < procentChange.size(); i++)
        sumPercents += procentChange[i];

    double PercentThreshold = ddOpts.percentThreshold;
    int countDays = ddOpts.countDays;

    data.countDays = 0;
    if(countDown >= countDays && fabs(sumPercents) >= PercentThreshold) {
        data.name = m_name;
        data.percentChange = sumPercents;
        data.countDays = countDown;
        qDebug()<<m_name<<QString::number(countDown)<<"days go down for " + QString::number(sumPercents,'g',2) + "%!";
    }
    if(countUp >= countDays && fabs(sumPercents) >= PercentThreshold) {
        data.name = m_name;
        data.percentChange = sumPercents;
        data.countDays = countUp;
        qDebug()<<m_name<<QString::number(countUp)<<"days go up for " + QString::number(sumPercents,'g',2) + "%!";
    }
}

int CCompany::getNumFinam()
{
    return m_numFinam;
}

void CCompany::setNumFinam(int numFinam)
{
    m_numFinam = numFinam;
}


