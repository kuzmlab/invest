#ifndef CALGDAYSCALP_H
#define CALGDAYSCALP_H

#include <QObject>
#include <QDebug>
#include "structs.h"


class CAlgDayScalp : public QObject
{
    Q_OBJECT
public:
    explicit CAlgDayScalp(QObject *parent = nullptr);

    void calc(QString ticker, QList<str_priceDay> dataPrices);

signals:

public slots:
};

#endif // CALGDAYSCALP_H
