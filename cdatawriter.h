#ifndef CDATAWRITER_H
#define CDATAWRITER_H

#include <QObject>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QFile>
#include <QStringList>
#include "ccompany.h"
#include "defines.h"
#include <QMapIterator>
#include "cfindata.h"

class CDataWriter : public QObject
{
    Q_OBJECT
public:
    explicit CDataWriter(QObject *parent = 0);

    QXmlStreamWriter stream;
    QXmlStreamReader reader;

    void saveData(QString nameFile);
    void readData(QString nameFile);

    void readFinamLinks(QString nameFile);
signals:

public slots:

};

#endif // CDATAWRITER_H
