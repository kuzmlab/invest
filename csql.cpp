#include "csql.h"

CSql::CSql(QObject *parent) :
    QObject(parent)
{
    createConnect();
}

CSql::~CSql()
{
    delete queryInsert;
    delete queryGet;
    delete queryCreate;
}

bool CSql::createTables()
{
    QSqlQuery query;
    QString str = "CREATE TABLE numFinam ("
                  "number INTEGER PRIMARY KEY NOT NULL,"
                  "name VARCHAR(50),"
                  "numFinam VARCHAR(50),"
                  "ticker VARCHAR(8),"
                  "linkInvesting VARCHAR(50)"
                  ");";
    if(!query.exec(str)) {
      //  qDebug()<<"unable to create table";
    }

    str = "CREATE TABLE currencyFinam ("
                  "number INTEGER PRIMARY KEY NOT NULL,"
                  "name VARCHAR(50),"
                  "numFinam VARCHAR(50),"
                  "ticker VARCHAR(8)"
                  ");";
    if(!query.exec(str)) {
      //  qDebug()<<"unable to create table";
    }

    str = "CREATE TABLE futuresFinam ("
                  "number INTEGER PRIMARY KEY NOT NULL,"
                  "name VARCHAR(50),"
                  "numFinam VARCHAR(50),"
                  "ticker VARCHAR(8)"
                  ");";
    if(!query.exec(str)) {
      //  qDebug()<<"unable to create table";
    }

    str = "CREATE TABLE indexesFinam ("
          "number INTEGER PRIMARY KEY NOT NULL,"
          "name VARCHAR(50),"
          "numFinam VARCHAR(50),"
          "ticker VARCHAR(8)"
          ");";
    if(!query.exec(str)) {
        //  qDebug()<<"unable to create table";
    }
    return true;
}


bool CSql::createTableCompany(const QString &ticker)
{
    QString str = "CREATE TABLE IF NOT EXISTS " + ticker + " ("
                  "open REAL,"
                  "close REAL,"
                  "low REAL,"
                  "high REAL,"
                  "volume REAL, "
                  "date TEXT UNIQUE"
                  ");";
    if(!queryCreate->exec(str)){
      //  qDebug()<<"unable to create table";
    }
    return true;
}

bool CSql::createTablePortfolio(const QString &user)
{
    QSqlQuery query;
    QString str = "CREATE TABLE " + user + " ("
                                             "name TEXT,"
                                             "volume INTEGER,"
                                             "price REAL,"
                                             "date TEXT"
                                             ");";
    if(!query.exec(str)) {
        //  qDebug()<<"unable to create table";
    }
    return true;
}


bool CSql::insertSharesInPortfolio(const QString &user, const QString &name, const int &volume, const double &price, const QString &date)
{
    QString strF = "INSERT INTO " + user + " (name, volume, price, date) "
                  "VALUES('%1', '%2', '%3', '%4');";

    QString str = strF.arg(name)
                      .arg(QString::number(volume))
                      .arg(QString::number(price))
                      .arg(date);
    QSqlQuery query;
    if(!query.exec(str)) {
        qDebug()<<"unable to insert"<<query.lastError();
    }

    return true;
}

void CSql::readPortfolio(const QString &namePortfolio, QList<CompanyPurchase_str> &data)
{
    QSqlQuery query;
    if(!query.exec("SELECT * FROM " + namePortfolio + ";")) {
        qDebug()<< "Unable to select";
        return;
    }
    QSqlRecord rec = query.record();

    while(query.next()) {
        CompanyPurchase_str tmpComp;
        tmpComp.name = query.value(rec.indexOf("name")).toString();
        tmpComp.volume = query.value(rec.indexOf("volume")).toInt();
        tmpComp.price = query.value(rec.indexOf("price")).toDouble();
        tmpComp.date = query.value(rec.indexOf("date")).toString();
        //проверяем для усреднения
        int flagAlreadyInData = 0, i = 0;
        for(CompanyPurchase_str cmp : data) {
            if(cmp.name == tmpComp.name) {
                cmp.price = (cmp.price*cmp.volume + tmpComp.price*tmpComp.volume)/(cmp.volume+tmpComp.volume);
                cmp.volume += tmpComp.volume;
                flagAlreadyInData = 1;
                data.replace(i, cmp);
            }
            i++;
        }
        if(!flagAlreadyInData)
            data.append(tmpComp);

    }
}


void CSql::getCompInPorftiolio(const QString &namePortfolio, const QString &nameComp, QList<CompanyPurchase_str> &data)
{
    QSqlQuery query;
    if(!query.exec("SELECT * FROM " + namePortfolio + " WHERE name = '" + nameComp + "';")) {
        qDebug()<< "Unable to select"<<namePortfolio<<nameComp;
        return;
    }
    QSqlRecord rec = query.record();

    while(query.next()) {
        CompanyPurchase_str tmpComp;
        tmpComp.name = query.value(rec.indexOf("name")).toString();
        tmpComp.volume = query.value(rec.indexOf("volume")).toInt();
        tmpComp.price = query.value(rec.indexOf("price")).toDouble();
        tmpComp.date = query.value(rec.indexOf("date")).toString();

        data.append(tmpComp);
    }
}

bool CSql::getVolumeCompIfBoughtToday(const QString &namePortfolio, const QString &nameComp, const QString &date, int &volume, double &price)
{
    QSqlQuery query;
    if(!query.exec("SELECT * FROM " + namePortfolio + " WHERE name = '" + nameComp + "' AND date = '" + date + "';")) {
        qDebug()<< "Unable to select"<<namePortfolio<<nameComp<<date;
        return false;
    }
    QSqlRecord rec = query.record();

    while(query.next()) {
        volume = query.value(rec.indexOf("volume")).toInt();
        price = query.value(rec.indexOf("price")).toDouble();
        return true;
    }
    return false;
}



bool CSql::insertDataFinam(const int &ID, const QString &name, const int &numFinam, const QString &ticker)
{
    QString strF = "INSERT INTO numFinam (number, name, numFinam, ticker) "
                  "VALUES(%1, '%2', '%3', '%4');";

    QString str = strF.arg(QString::number(ID))
                      .arg(name)
                      .arg(QString::number(numFinam))
                      .arg(ticker);
    QSqlQuery query;
    if(!query.exec(str)) {
       // qDebug()<<"unable to insert";
    }
    return true;
}

bool CSql::insertDataPrice(const str_priceDay &dataPrice)
{
    queryInsert->bindValue(":open", dataPrice.open);
    queryInsert->bindValue(":close", dataPrice.close);
    queryInsert->bindValue(":low", dataPrice.low);
    queryInsert->bindValue(":high", dataPrice.high);
    queryInsert->bindValue(":volume", dataPrice.volume);
    queryInsert->bindValue(":date", dataPrice.date);

    if(!queryInsert->exec()) {
         int numError = queryInsert->lastError().number();
         if(numError == 19)//unique data already in table
             return false;

         qDebug()<<"insterDataPrice error "<<queryInsert->lastError();
    }

    return true;
}

bool CSql::createConnect()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("shares.db");
    db.setUserName("Oleg");
    db.setHostName("Kuzmichev");
    db.setPassword("krekan123");
    if(!db.open()) {
        qDebug()<<"cannot open db";
        return false;
    }

    db.exec("PRAGMA synchronous = OFF");
    db.exec("PRAGMA journal_mode = MEMORY");
    queryInsert = new QSqlQuery(db);
    queryGet    = new QSqlQuery(db);
    queryCreate = new QSqlQuery(db);

    createTables();

    return true;
}

bool CSql::dropTable(const QString &nameTable)
{
    QSqlQuery query;
    if(!query.exec("DROP TABLE " + nameTable + ";")) {
        qDebug()<< "Unable to drop";
        return false;
    }
    return true;
}

bool CSql::stringToDate(const QString &strDate, QDate &date)
{
    QStringList tmp = strDate.split(".");
    if(tmp.size() != 3)
        return false;

    return date.setDate(tmp.at(2).toInt(), tmp.at(1).toInt(), tmp.at(0).toInt());
}

void CSql::getQuarterByMonth(const int &month, int &quarter)
{
    if(month >= 1 && month <=3)
        quarter = 1;
    else if(month >= 4 && month <=6)
        quarter = 2;
    else if(month >= 7 && month <=9)
        quarter = 3;
    else if(month >= 10 && month <=12)
        quarter = 4;
}

bool CSql::updateCmpPorftolioVolume(const QString &nameTable, const QString &nameCmp, const int &volume, const QString &date)
{
    QSqlQuery query;
    if(!query.exec("UPDATE " + nameTable + " SET volume = "+ QString::number(volume) + " WHERE name = '" + nameCmp + "' AND date = '" + date + "';")) {
        qDebug()<< "Unable to update"<<nameTable<<nameCmp<<volume<<date;
        return false;
    }
    return true;
}

bool CSql::updateCmpPorftolioPrice(const QString &nameTable, const QString &nameCmp, const double &price, const QString &date)
{
    QSqlQuery query;
    if(!query.exec("UPDATE " + nameTable + " SET price = "+ QString::number(price) + " WHERE name = '" + nameCmp + "' AND date = '" + date + "';")) {
        qDebug()<< "Unable to update"<<nameTable<<nameCmp<<price<<date;
        return false;
    }
    return true;
}

bool CSql::delCmpPorftolio(const QString &nameTable, const QString &nameCmp)
{
    QSqlQuery query;
    if(!query.exec("DELETE FROM " + nameTable + " WHERE name = '" + nameCmp + "';")) {
        qDebug()<< "Unable to delete"<<nameTable<<nameCmp;
        return false;
    }
    return true;
}

bool CSql::delCmpPorftolioByDate(const QString &nameTable, const QString &nameCmp, const QString &date)
{
    QSqlQuery query;
    if(!query.exec("DELETE FROM " + nameTable + " WHERE name = '" + nameCmp + "' AND date = '" + date + "';")) {
        qDebug()<< "Unable to delete by date"<<nameTable<<nameCmp;
        return false;
    }
    return true;
}

bool CSql::delDatePriceByDay(const QString &nameTable, const QString &date)
{
    QSqlQuery query;
    if(!query.exec("DELETE FROM " + nameTable + " WHERE date = '" + date + "';")) {
        qDebug()<< "Unable to delete by date"<<nameTable<<date;
        return false;
    }
    return true;
}

//void CSql::readFinamLinks(QList<CCompany*> *data)
//{
//    QSqlQuery query;
//    if(!query.exec("SELECT * FROM numFinam;")) {
//        qDebug()<< "Unable to select";
//        return;
//    }
//    QSqlRecord rec = query.record();
//    int number = -1, numFinam = -1;
//    QString ticker = "", name = "";

//    while(query.next()) {
//        number = query.value(rec.indexOf("number")).toInt();
//        numFinam = query.value(rec.indexOf("numFinam")).toInt();
//        ticker = query.value(rec.indexOf("ticker")).toString();
//        name = query.value(rec.indexOf("name")).toString();
//        CCompany *tmpComp = new CCompany(this, name, ticker, numFinam);
//        data->append(tmpComp);
//    }
//}

template <class T>
void CSql::readFinamLinks(QList<T> *data, const QString &nameTable)
{
    QSqlQuery query;
    if(!query.exec("SELECT * FROM " + nameTable + ";")) {
        qDebug()<< "Unable to select";
        return;
    }
    QSqlRecord rec = query.record();

    while(query.next()) {
        T tmp(query.value(rec.indexOf("name")).toString(),
              query.value(rec.indexOf("ticker")).toString(),
              query.value(rec.indexOf("numFinam")).toInt(),
              query.value(rec.indexOf("linkInvesting")).toString());
        data->append(tmp);
    }

}

template void CSql::readFinamLinks<CCompany>(QList<CCompany> *data, const QString &nameTable);
//template void CSql::readFinamLinks<Currency>(QList<Currency> *data, QString nameTable);
//template void CSql::readFinamLinks<double>(double);

bool CSql::getLastDateByTicker(const QString &ticker, QDate &date)
{
    if(!queryGet->exec("SELECT * FROM " + ticker + ";")) {
        qDebug()<< "Unable to select "<< ticker;
        return false;
    }
    QSqlRecord rec = queryGet->record();
    if(!queryGet->isActive()) {
        qDebug()<<"query get not active! ticker:"<<ticker;
        return false;
    }
    if(!queryGet->last()) {
        qDebug()<<"query.last is null ticker:"<<ticker;
        return true;
    }
    QString tmpDate = queryGet->value(rec.indexOf("date")).toString();
    if(!stringToDate(tmpDate, date)) {
        qDebug()<<"bad last date:"<<tmpDate<<ticker;
        return false;
    }

    return true;
}

void CSql::getDailyPriceHistoryByTicker(const QDate &dateFrom, const QDate &dateTo, const QString &ticker, QList<str_priceDay> &data)
{
    QSqlQuery query;
    if(!query.exec("SELECT * FROM " + ticker + ";")) {
        qDebug()<< "Unable to select "<< ticker;
        return;
    }
    QSqlRecord rec = query.record();
    str_priceDay tmpDP;
    data.clear();

    while(query.next()) {
        tmpDP.open = query.value(rec.indexOf("open")).toDouble();
        tmpDP.close = query.value(rec.indexOf("close")).toDouble();
        tmpDP.low = query.value(rec.indexOf("low")).toDouble();
        tmpDP.high = query.value(rec.indexOf("high")).toDouble();
        tmpDP.volume = query.value(rec.indexOf("volume")).toDouble();
        tmpDP.date = query.value(rec.indexOf("date")).toString();

        QDate date;
        if(!stringToDate(tmpDP.date, date)) {
            qDebug()<<"error date in stringToDate -"<<tmpDP.date<<ticker;
            continue;
        }
        QDateTime tmpDateTime(date);
        tmpDP.dateSecs = tmpDateTime.toSecsSinceEpoch();

        if(date < dateTo && date > dateFrom)
            data.append(tmpDP);
    }
}

void CSql::getWeeklyPriceHistoryByTicker(const QDate &dateFrom, const QDate &dateTo, const QString &ticker, QList<str_priceDay> &data)
{
    int prevDayOfWeek = -1;
    double prevClose = 0;
    str_priceDay weekDP;
    data.clear();
    QSqlQuery query;
    if(!query.exec("SELECT * FROM " + ticker + ";")) {
        qDebug()<< "Unable to select "<< ticker;
        return;
    }
    QSqlRecord rec = query.record();
    while(query.next()) {
        str_priceDay tmpDP;
        tmpDP.open = query.value(rec.indexOf("open")).toDouble();
        tmpDP.close = query.value(rec.indexOf("close")).toDouble();
        tmpDP.low = query.value(rec.indexOf("low")).toDouble();
        tmpDP.high = query.value(rec.indexOf("high")).toDouble();
        tmpDP.volume = query.value(rec.indexOf("volume")).toDouble();
        tmpDP.date = query.value(rec.indexOf("date")).toString();

        QDate tmpDate;
        if(!stringToDate(tmpDP.date, tmpDate)) {
            qDebug()<<"error date in stringToDate -"<<tmpDP.date<<ticker;
            continue;
        }
        QDateTime tmpDateTime(tmpDate);
        tmpDP.dateSecs = tmpDateTime.toSecsSinceEpoch();

        if(prevDayOfWeek == -1 || prevDayOfWeek == 1) {
            if(prevDayOfWeek != -1 && tmpDate < dateTo && tmpDate > dateFrom) {
                weekDP.close = prevClose;
                data.append(weekDP);
            }
            weekDP.open = tmpDP.open;
            weekDP.close = tmpDP.close;
            weekDP.low = tmpDP.low;
            weekDP.high = tmpDP.high;
            weekDP.volume = tmpDP.volume;
        }
        else {
            if(tmpDP.high > weekDP.high)
                weekDP.high = tmpDP.high;
            if(tmpDP.low < weekDP.low)
                weekDP.low = tmpDP.low;
            weekDP.volume += tmpDP.volume;
        }
        prevClose = tmpDP.close;
        prevDayOfWeek = tmpDate.dayOfWeek();
        if(!prevDayOfWeek)
            qDebug()<<"invalid dayOfWeek:"<<tmpDate;
    }

    if(prevDayOfWeek != 5) {
        weekDP.close = prevClose;
        data.append(weekDP);
    }
}

void CSql::getMonthlyPriceHistoryByTicker(const QDate &dateFrom, const QDate &dateTo, const QString &ticker, QList<str_priceDay> &data)
{
    int month = -1, prevMonth = -1, lastMonthAppended = -1;
    double prevClose = 0;
    str_priceDay monthDP;
    data.clear();
    QSqlQuery query;
    if(!query.exec("SELECT * FROM " + ticker + ";")) {
        qDebug()<< "Unable to select "<< ticker;
        return;
    }
    QSqlRecord rec = query.record();
    while(query.next()) {
        str_priceDay tmpDP;
        tmpDP.open = query.value(rec.indexOf("open")).toDouble();
        tmpDP.close = query.value(rec.indexOf("close")).toDouble();
        tmpDP.low = query.value(rec.indexOf("low")).toDouble();
        tmpDP.high = query.value(rec.indexOf("high")).toDouble();
        tmpDP.volume = query.value(rec.indexOf("volume")).toDouble();
        tmpDP.date = query.value(rec.indexOf("date")).toString();

        QDate tmpDate;
        if(!stringToDate(tmpDP.date, tmpDate)) {
            qDebug()<<"error date in stringToDate -"<<tmpDP.date<<ticker;
            continue;
        }
        month = tmpDate.month();
        QDateTime tmpDateTime(tmpDate);
        tmpDP.dateSecs = tmpDateTime.toSecsSinceEpoch();

        if(prevMonth == -1 || month != prevMonth) {
            if(prevMonth != -1 && tmpDate < dateTo && tmpDate > dateFrom) {
                monthDP.close = prevClose;
                lastMonthAppended = prevMonth;
                data.append(monthDP);
            }
            monthDP.open = tmpDP.open;
            monthDP.close = tmpDP.close;
            monthDP.low = tmpDP.low;
            monthDP.high = tmpDP.high;
            monthDP.volume = tmpDP.volume;
        }
        if(month == prevMonth) {
            if(tmpDP.high > monthDP.high)
                monthDP.high = tmpDP.high;
            if(tmpDP.low < monthDP.low)
                monthDP.low = tmpDP.low;
            monthDP.volume += tmpDP.volume;
        }

        prevMonth = month;
        prevClose = tmpDP.close;
    }

    if(month != lastMonthAppended) {
        monthDP.close = prevClose;
        data.append(monthDP);
    }
}

void CSql::getQuaterlyPriceHistoryByTicker(const QDate &dateFrom, const QDate &dateTo, const QString &ticker, QList<str_priceDay> &data)
{
    int quarter = -1, prevQuarter = -1, lastQuarterAppended = -1;
    double prevClose = 0;
    str_priceDay quarterDP;
    data.clear();
    QSqlQuery query;
    if(!query.exec("SELECT * FROM " + ticker + ";")) {
        qDebug()<< "Unable to select "<< ticker;
        return;
    }
    QSqlRecord rec = query.record();
    while(query.next()) {
        str_priceDay tmpDP;
        tmpDP.open = query.value(rec.indexOf("open")).toDouble();
        tmpDP.close = query.value(rec.indexOf("close")).toDouble();
        tmpDP.low = query.value(rec.indexOf("low")).toDouble();
        tmpDP.high = query.value(rec.indexOf("high")).toDouble();
        tmpDP.volume = query.value(rec.indexOf("volume")).toDouble();
        tmpDP.date = query.value(rec.indexOf("date")).toString();

        QDate tmpDate;
        if(!stringToDate(tmpDP.date, tmpDate)) {
            qDebug()<<"error date in stringToDate -"<<tmpDP.date<<ticker;
            continue;
        }
        getQuarterByMonth(tmpDate.month(), quarter);

        QDateTime tmpDateTime(tmpDate);
        tmpDP.dateSecs = tmpDateTime.toSecsSinceEpoch();

        if(prevQuarter == -1 || quarter != prevQuarter) {
            if(prevQuarter != -1 && tmpDate < dateTo && tmpDate > dateFrom) {
                quarterDP.close = prevClose;
                lastQuarterAppended = quarter;
                data.append(quarterDP);
            }
            quarterDP.open = tmpDP.open;
            quarterDP.close = tmpDP.close;
            quarterDP.low = tmpDP.low;
            quarterDP.high = tmpDP.high;
            quarterDP.volume = tmpDP.volume;
        }
        if(quarter == prevQuarter) {
            if(tmpDP.high > quarterDP.high)
                quarterDP.high = tmpDP.high;
            if(tmpDP.low < quarterDP.low)
                quarterDP.low = tmpDP.low;
            quarterDP.volume += tmpDP.volume;
        }


        prevQuarter = quarter;
        prevClose = tmpDP.close;
    }

    if(quarter != lastQuarterAppended) {
        quarterDP.close = prevClose;
        data.append(quarterDP);
    }
}

void CSql::getYearlyPriceHistoryByTicker(const QDate &dateFrom, const QDate &dateTo, const QString &ticker, QList<str_priceDay> &data)
{
    int year = -1, prevYear = -1, lastYearAppended = -1;
    double prevClose = 0;
    str_priceDay yearDP;
    data.clear();
    QSqlQuery query;
    if(!query.exec("SELECT * FROM " + ticker + ";")) {
        qDebug()<< "Unable to select "<< ticker;
        return;
    }
    QSqlRecord rec = query.record();
    while(query.next()) {
        str_priceDay tmpDP;
        tmpDP.open = query.value(rec.indexOf("open")).toDouble();
        tmpDP.close = query.value(rec.indexOf("close")).toDouble();
        tmpDP.low = query.value(rec.indexOf("low")).toDouble();
        tmpDP.high = query.value(rec.indexOf("high")).toDouble();
        tmpDP.volume = query.value(rec.indexOf("volume")).toDouble();
        tmpDP.date = query.value(rec.indexOf("date")).toString();

        QDate tmpDate;
        if(!stringToDate(tmpDP.date, tmpDate)) {
            qDebug()<<"error date in stringToDate -"<<tmpDP.date<<ticker;
            continue;
        }
        year = tmpDate.year();
        QDateTime tmpDateTime(tmpDate);
        tmpDP.dateSecs = tmpDateTime.toSecsSinceEpoch();

        if(prevYear == -1 || year != prevYear) {
            if(prevYear != -1 && tmpDate < dateTo && tmpDate > dateFrom) {
                yearDP.close = prevClose;
                lastYearAppended = prevYear;
                data.append(yearDP);
            }
            yearDP.open   = tmpDP.open;
            yearDP.close  = tmpDP.close;
            yearDP.low    = tmpDP.low;
            yearDP.high   = tmpDP.high;
            yearDP.volume = tmpDP.volume;
        }
        if(year == prevYear) {
            if(tmpDP.high > yearDP.high)
                yearDP.high = tmpDP.high;
            if(tmpDP.low < yearDP.low)
                yearDP.low = tmpDP.low;
            yearDP.volume += tmpDP.volume;
        }


        prevYear = year;
        prevClose = tmpDP.close;
    }

    if(year != lastYearAppended) {
        yearDP.close = prevClose;
        data.append(yearDP);
    }
}



void CSql::prepareQueryInsert(const QString &ticker)
{
    if(!queryInsert->prepare("INSERT INTO " + ticker + " (open, close, low, high, volume, date) "
                        "VALUES (:open, :close, :low, :high, :volume, :date)"))
        qDebug()<<"unable to prepare "<<ticker;
}

