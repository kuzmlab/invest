#include "cfindata.h"

CFinData::CFinData(QObject *parent) :
    QObject(parent)
{
    EV = 0;
    EBITDA = 0;
    debt = 0;
    equityMarket = 0;
    minorityInterest = 0;
    valueAssociate = 0;
    cash = 0;
    ev_ebitda = 0;
    capitalization = 0;
    debt_ebitda = 0;
    priceShare = 0;
    countShares = 0;
    freeFloat = 0;
    netDebt = 0;
    profitBeforeTaxes = 0;
    percentPush = 0;
    amortization = 0;
    cleanProfit = 0;
    typeVal = VAL_DOLLAR;
    countSharesPreffered = 0;
    priceSharePreff = 0;
    ev_profit = 0;
    p_e = 0;
    year = 2016;
    quarter = 3;
}

void CFinData::analyze()
{
    //все подсчеты в млрд рублей
    //рыночная капитализация
    capitalization = countShares*priceShare/1000000000.;
    //чистый долг - debt+cash
    netDebt = (debt - cash)/1000.;
    // if в млн долларах - переводим
//    if(typeVal == VAL_DOLLAR)
//        netDebt = netDebt*priceDollar;
    //доля миноритариев
    minorityInterest = freeFloat/100. * capitalization;
    prefferedInterest = countShares * priceSharePreff / 1000000000.;
    EV = capitalization + netDebt + minorityInterest + prefferedInterest;
  //  qDebug()<<name<<"marketCap:"<<capitalization<<"netDebt:"<<netDebt<<"minor:"<<minorityInterest<<"EV:"<<EV<<"prefInt:"<<prefferedInterest;

    //MSFO
    EBITDA = profitBeforeTaxes + percentPush + amortization;
    //в млрд рублей
    EBITDA = EBITDA/1000.;
//    if(typeVal == VAL_DOLLAR)
//        EBITDA = EBITDA*priceDollar;
//    qDebug()<<"profit:"<<profitBeforeTaxes<<"amortization:"<<amortization<<"procent:"<<percentPush<<"EBITDA:"<<EBITDA;

    ev_ebitda = EV / EBITDA;
    ev_profit = EV / (cleanProfit/1000.);
    debt_ebitda = netDebt / EBITDA;
    p_e = capitalization / (cleanProfit/1000.);

 //   qDebug()<<"ev_ebitda:"<<ev_ebitda<<"debt_ebitda:"<<debt_ebitda;

    int stop;
    stop = 0;
}

