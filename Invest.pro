#-------------------------------------------------
#
# Project created by QtCreator 2016-12-10T13:31:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport network
LIBS += -Llibeay32 -Lssleay32

INCLUDEPATH += C:/OpenSSL-Win32/include


TARGET = Invest
TEMPLATE = app

QT += sql

CONFIG += c++11
QMAKE_CXXFLAGS += -std=gnu++11


SOURCES += main.cpp\
        dlg/mainwindow.cpp \
    dlg/qcustomplot.cpp \
    ccompany.cpp \
    dlg/adddatadlg.cpp \
    cdatawriter.cpp \
    dlg/deldatadlg.cpp \
    defines.cpp \
    cdatagetter.cpp \
    cfindata.cpp \
    dlg/daydatadlg.cpp \
    csql.cpp \
    calgdayscalp.cpp \
    dlg/todaypricesdlg.cpp \
    dlg/portfoliosharesdlg.cpp \
    dlg/purchasedlg.cpp \
    dlg/quotesgraphicdlg.cpp

HEADERS  += dlg/mainwindow.h \
    dlg/qcustomplot.h \
    structs.h \
    defines.h \
    ccompany.h \
    dlg/adddatadlg.h \
    cdatawriter.h \
    dlg/deldatadlg.h \
    cdatagetter.h \
    cfindata.h \
    dlg/daydatadlg.h \
    csql.h \
    calgdayscalp.h \
    dlg/todaypricesdlg.h \
    dlg/portfoliosharesdlg.h \
    dlg/purchasedlg.h \
    dlg/quotesgraphicdlg.h

FORMS    += dlg/mainwindow.ui \
    dlg/adddatadlg.ui \
    dlg/deldatadlg.ui \
    dlg/daydatadlg.ui \
    dlg/todaypricesdlg.ui \
    dlg/portfoliosharesdlg.ui \
    dlg/purchasedlg.ui
