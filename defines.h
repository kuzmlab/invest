#ifndef DEFINES_H
#define DEFINES_H

#include <QString>
#include "structs.h"

enum {
    TYPE_RAW,
    TYPE_PHARM,
    TYPE_ELEKTRO,
    TYPE_IT,
    TYPE_TRANSPORT,
    TYPE_RETAIL,
    TYPE_TELEPHONE,
    TYPE_BANK,
    TYPE_AGRO,
    TYPE_AUTO,
    TYPE_OILGAS,
    TYPE_ALL
};

enum {
    VAL_DOLLAR,
    VAL_RUBLE,
    VAL_EURO
};

QString getNameByType(int type);
int getTypeByName(QString name);

template <class T>
bool greaterThan(const T &d1, const T &d2)
{
    return d1.percentChange < d2.percentChange;
}
template <class T>
bool lessThan(const T &d1, const T &d2)
{
    return d1.percentChange > d2.percentChange;
}


#endif // DEFINES_H

