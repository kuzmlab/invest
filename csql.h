#ifndef CSQL_H
#define CSQL_H

#include <QObject>
#include <QtSql>
#include <QStringList>
#include "ccompany.h"

class CSql : public QObject
{
    Q_OBJECT
public:
    explicit CSql(QObject *parent = nullptr);
    ~CSql();

    bool createConnect();
    bool createTablePortfolio(const QString &user);
    bool createTables();
    bool createTableCompany(const QString &ticker);

    bool insertDataFinam(const int &ID, const QString &name, const int &numFinam, const QString &ticker);
    bool insertDataPrice(const str_priceDay &dataPrice);
    void prepareQueryInsert(const QString &ticker);
    bool insertSharesInPortfolio(const QString &user, const QString &name, const int &volume, const double &price, const QString &date);

    template <class T>
    void readFinamLinks(QList<T> *data, const QString &nameTable);

    bool updateCmpPorftolioVolume(const QString &nameTable, const QString &nameCmp, const int &volume, const QString &date);
    bool updateCmpPorftolioPrice(const QString &nameTable, const QString &nameCmp, const double &price, const QString &date);

    void readPortfolio(const QString &namePortfolio, QList<CompanyPurchase_str> &data);
    void getDailyPriceHistoryByTicker(const QDate &dateFrom, const QDate &dateTo, const QString &ticker, QList<str_priceDay> &data);
    void getWeeklyPriceHistoryByTicker(const QDate &dateFrom, const QDate &dateTo, const QString &ticker, QList<str_priceDay> &data);
    void getMonthlyPriceHistoryByTicker(const QDate &dateFrom, const QDate &dateTo, const QString &ticker, QList<str_priceDay> &data);
    void getQuaterlyPriceHistoryByTicker(const QDate &dateFrom, const QDate &dateTo, const QString &ticker, QList<str_priceDay> &data);
    void getYearlyPriceHistoryByTicker(const QDate &dateFrom, const QDate &dateTo, const QString &ticker, QList<str_priceDay> &data);
    void getCompInPorftiolio(const QString &namePortfolio, const QString &nameComp, QList<CompanyPurchase_str> &data);
    bool getVolumeCompIfBoughtToday(const QString &namePortfolio, const QString &nameComp, const QString &date, int &volume, double &price);
    bool getLastDateByTicker(const QString &ticker, QDate &date);

    bool delCmpPorftolio(const QString &nameTable, const QString &nameCmp);
    bool delCmpPorftolioByDate(const QString &nameTable, const QString &nameCmp, const QString &date);
    bool delDatePriceByDay(const QString &nameTable, const QString &date);
    bool dropTable(const QString &nameTable);


private:
    QSqlQuery *queryInsert;
    QSqlQuery *queryGet;
    QSqlQuery *queryCreate;

    bool stringToDate(const QString &strDate, QDate &date);
    void getQuarterByMonth(const int &month, int &quarter);

signals:

public slots:

};

#endif // CSQL_H
