#ifndef DELDATADLG_H
#define DELDATADLG_H

#include <QDialog>
#include "ccompany.h"
#include "defines.h"

namespace Ui {
class DelDataDlg;
}

class DelDataDlg : public QDialog
{
    Q_OBJECT

public:
    explicit DelDataDlg(QWidget *parent = 0);
    ~DelDataDlg();


public slots:
    void changeType(QString type);

private slots:
    void on_pushButton_clicked();

private:
    Ui::DelDataDlg *ui;
};

#endif // DELDATADLG_H
