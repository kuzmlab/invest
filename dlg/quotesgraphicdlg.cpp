#include "quotesgraphicdlg.h"

QuotesGraphicDlg::QuotesGraphicDlg(QWidget *parent) : QWidget(parent)
{

}

void QuotesGraphicDlg::draw(const QList<str_priceDay> &pricesCompany)
{
    if(pricesCompany.isEmpty()) {
        qDebug()<<"error, pricesCompany is empty!";
        return;
    }
    plot->clearItems();
    plot->legend->setVisible(false);

    qint64 binSize = 3600*24;
    qint64 secsSinceEpoch = pricesCompany.first().dateSecs;
    QVector<QCPFinancialData> qFinData;
    int i = 0;
    for(str_priceDay tmpDP : pricesCompany) {
        QCPFinancialData tmpFin(secsSinceEpoch + binSize*i, tmpDP.open,tmpDP.high,tmpDP.low,tmpDP.close);
        qFinData.append(tmpFin);
        i++;
    }

    candlesticks->data()->set(qFinData);
    volumeNeg->clearData();
    volumePos->clearData();
    i = 0;
    for(str_priceDay tmpDP : pricesCompany) {
        (tmpDP.close - tmpDP.open < 0 ? volumeNeg : volumePos)->addData(secsSinceEpoch + binSize*i, tmpDP.volume);
        i++;
    }

    plot->rescaleAxes();
    plot->replot();

    //add mark
//    int counterX = 0, closePrice = 0;
//    i = 0;
//    for(str_priceDay tmpDP : pricesCompany) {
//        if(tmpDP.date == "4.3.2019") {
//            closePrice = tmpDP.close;
//            counterX = i;
//            break;
//        }
//        i++;
//    }

//    QCPItemText *textLabel = new QCPItemText(plot);
//    textLabel->setPositionAlignment(Qt::AlignTop|Qt::AlignHCenter);
//    textLabel->position->setCoords(secsSinceEpoch + binSize*counterX, 400); // place position at center/top of axis rect
//    textLabel->setText("ИИ №73");
//    textLabel->setFont(QFont(font().family(), 12)); // make font a bit larger
////   textLabel->setPen(QPen(Qt::black)); // show black border around text

//    // add the arrow:
//    QCPItemLine *arrow = new QCPItemLine(plot);
//    arrow->start->setParentAnchor(textLabel->bottom);
//    arrow->end->setCoords(secsSinceEpoch + binSize*counterX, closePrice);
//    arrow->setHead(QCPLineEnding::esSpikeArrow);
}

void QuotesGraphicDlg::init()
{
    this->resize(1100,500);
    plot = new QCustomPlot(this);
    plot->resize(1100,500);
    //ширина свечи
    int binSize = 3600*24;
    plot->legend->setVisible(false);

    candlesticks = new QCPFinancial(plot->xAxis, plot->yAxis);
    candlesticks->setName("SIBN");
    candlesticks->setChartStyle(QCPFinancial::csCandlestick);
    candlesticks->setWidth(binSize);
    candlesticks->setTwoColored(true);
    candlesticks->setBrushPositive(QColor(100, 180, 110));
    candlesticks->setBrushNegative(QColor(180, 90, 90));
    candlesticks->setPenPositive(QPen(QColor(0, 0, 0)));
    candlesticks->setPenNegative(QPen(QColor(0, 0, 0)));

    // create bottom axis rect for volume bar chart:
    QCPAxisRect *volumeAxisRect = new QCPAxisRect(plot);
    plot->plotLayout()->addElement(1, 0, volumeAxisRect);
    volumeAxisRect->setMaximumSize(QSize(QWIDGETSIZE_MAX, 100));
    volumeAxisRect->axis(QCPAxis::atBottom)->setLayer("axes");
    volumeAxisRect->axis(QCPAxis::atBottom)->grid()->setLayer("grid");
    // bring bottom and main axis rect closer together:
    plot->plotLayout()->setRowSpacing(0);
    volumeAxisRect->setAutoMargins(QCP::msLeft|QCP::msRight|QCP::msBottom);
    volumeAxisRect->setMargins(QMargins(0, 0, 0, 0));
    // create two bar plottables, for positive (green) and negative (red) volume bars:
    plot->setAutoAddPlottableToLegend(false);
    volumePos = new QCPBars(volumeAxisRect->axis(QCPAxis::atBottom), volumeAxisRect->axis(QCPAxis::atLeft));
    volumeNeg = new QCPBars(volumeAxisRect->axis(QCPAxis::atBottom), volumeAxisRect->axis(QCPAxis::atLeft));

    volumePos->setWidth(binSize);
    volumePos->setPen(Qt::NoPen);
    volumePos->setBrush(QColor(100, 180, 110));
    volumeNeg->setWidth(binSize);
    volumeNeg->setPen(Qt::NoPen);
    volumeNeg->setBrush(QColor(180, 90, 90));

    // interconnect x axis ranges of main and bottom axis rects:
    connect(plot->xAxis, SIGNAL(rangeChanged(QCPRange)), volumeAxisRect->axis(QCPAxis::atBottom), SLOT(setRange(QCPRange)));
    connect(volumeAxisRect->axis(QCPAxis::atBottom), SIGNAL(rangeChanged(QCPRange)), plot->xAxis, SLOT(setRange(QCPRange)));
    //    connect(plot, &QCustomPlot::mouseRelease, plot, &QCustomPlot::update);
    // configure axes of both main and bottom axis rect:
    QSharedPointer<QCPAxisTickerDateTime> dateTimeTicker(new QCPAxisTickerDateTime);
    dateTimeTicker->setDateTimeSpec(Qt::UTC);
    dateTimeTicker->setDateTimeFormat("dd MMMM");
    volumeAxisRect->axis(QCPAxis::atBottom)->setTicks(false);
    volumeAxisRect->axis(QCPAxis::atBottom)->setTickLabels(false);
    //   volumeAxisRect->axis(QCPAxis::atBottom)->setTicker(dateTimeTicker);
    //  volumeAxisRect->axis(QCPAxis::atBottom)->setTickLabelRotation(15);
    plot->xAxis->setBasePen(Qt::NoPen);
    plot->xAxis->setTickLabels(false);
    plot->xAxis->setTicks(false); // only want vertical grid in main axis rect, so hide xAxis backbone, ticks, and labels
    plot->xAxis->setTicker(dateTimeTicker);
    //    plot->xAxis->scaleRange(1.1, plot->xAxis->range().center());
    //   plot->yAxis->scaleRange(1.1, plot->yAxis->range().center());
    plot->rescaleAxes();
    //
    // make axis rects' left side line up:
    QCPMarginGroup *group = new QCPMarginGroup(plot);
    plot->axisRect()->setMarginGroup(QCP::msLeft|QCP::msRight, group);
    volumeAxisRect->setMarginGroup(QCP::msLeft|QCP::msRight, group);

 //   plot->setInteractions(QCP::iRangeZoom | QCP::iRangeDrag);
    plot->axisRect()->setRangeDrag(Qt::Horizontal);

    plot->show();

    connect(plot, &QCustomPlot::mouseWheel, this, &QuotesGraphicDlg::wheelChange);

}

//void MainWindow::drawGraphicFA()
//{
////    drawBackEnd();
////    painter.begin(this);
////    painter.setRenderHint(QPainter::HighQualityAntialiasing);

////    QFont tmpFont;
////    tmpFont.setPixelSize(12);
////    painter.setFont(tmpFont);
////    painter.setPen(Qt::gray);
////    int sizeX = 1100, sizeY = 580;
////    int x0 = 50, y0 = 500;
////    int xGraph = sizeX - x0;
////    int yGraph = sizeY - y0;
////    int sizeXgraph = xGraph - x0;
////    int sizeYgraph = y0 - yGraph;

////    //10 вертикальных
////    int stepX = sizeXgraph / 10;
////    int stepY = sizeYgraph / 6;
////    int tmpOffset = stepX;
////    for(int i = 0; i < 10; i++) {
////        painter.drawLine(x0 + tmpOffset, y0, x0 + tmpOffset, yGraph);
////        tmpOffset+=stepX;
////    }

////    painter.drawText(3, y0/2, "долг\\");
////    painter.drawText(3, y0/2+15, "EBITDA");
////    painter.drawText(xGraph/2, y0 + 40, "EV\\EBITDA");

////    int tmpType = getTypeByName(ui->comboBox->currentText());
////    if(!m_Data.contains(tmpType)) {
////         painter.end();
////         return;
////    }
////    painter.setPen(Qt::black);
////    //масштабы
////    double maxX = 0;
////    double maxY = 0;
////    double maxCap = 0;
////    for(int i = 0; i < m_Data[tmpType].size(); i++) {
////        getMaxParam(maxX, m_Data[tmpType][i]->m_dataComp[year][quarter]->ev_ebitda);
////        getMaxParam(maxY, m_Data[tmpType][i]->m_dataComp[year][quarter]->debt_ebitda);
////        getMaxParam(maxCap, m_Data[tmpType][i]->m_dataComp[year][quarter]->capitalization);
////    }
////    //осьХ
////    tmpOffset = stepX;
////    double stepMasht = maxX/10.;
////    double tmpMasht = stepMasht;
////    for(int i = 1; i < 10; i++) {
////        QString tmpStr = QString::number(tmpMasht);
////        int tmpInt = tmpStr.indexOf(".");
////        tmpStr = tmpStr.left(tmpInt + 2);
////        painter.drawText(x0 + tmpOffset - tmpStr.count() - 2, y0 + 20, tmpStr);
////        tmpOffset += stepX;
////        tmpMasht += stepMasht;
////    }
////    //осьY
////    tmpOffset = stepY;
////    stepMasht = maxY/6.;
////    tmpMasht = stepMasht;
////    for(int i = 1; i < 6; i++) {
////        QString tmpStr = QString::number(tmpMasht);
////        int tmpInt = tmpStr.indexOf(".");
////        tmpStr = tmpStr.left(tmpInt + 2);
////        painter.drawText(x0 - 23, y0 - tmpOffset + 2, tmpStr);
////        tmpOffset += stepY;
////        tmpMasht += stepMasht;
////    }

////    for(int i = 0; i < m_Data[tmpType].size(); i++) {
////        int x = getSize(m_Data[tmpType][i]->m_dataComp[year][quarter]->ev_ebitda, (double)sizeXgraph, maxX);
////        int y = y0 - getSize(m_Data[tmpType][i]->m_dataComp[year][quarter]->debt_ebitda, (double)sizeYgraph, maxY);
////        int size = getSize(m_Data[tmpType][i]->m_dataComp[year][quarter]->capitalization, 100, maxCap);

////        if(x < x0)
////            x = x0;
////        if(y > y0)
////            y = y0;

////       // painter.setBrush(Qt::red);
////        painter.drawEllipse(x,y, 1, 1);
////        painter.drawEllipse(x - size/2, y - size/2, size, size);
////        painter.drawText(x, y, m_Data[tmpType][i]->m_name);
////    }

////    painter.end();
//}

//void MainWindow::drawBackEnd()
//{
//    painter.begin(this);
//    painter.setRenderHint(QPainter::HighQualityAntialiasing);
//    painter.setPen(Qt::white);
//    int sizeX = 1100, sizeY = 580;
//    painter.fillRect(0, 0, sizeX, sizeY, Qt::white);

//    QFont tmpFont;
//    tmpFont.setPixelSize(12);
//    painter.setFont(tmpFont);

//    painter.setPen(Qt::gray);
//    int x0 = 50, y0 = 500;
//    int xGraph = sizeX - x0;
//    int yGraph = sizeY - y0;
//    int sizeYgraph = y0 - yGraph;

//    //оси
//    painter.drawLine(x0, y0, xGraph, y0);
//    painter.drawLine(x0, y0, x0, yGraph);

//    //6 горизонтальных
//    int stepY = sizeYgraph / 6;
//    int tmpOffset = stepY;
//    for(int i = 0; i < 6; i++) {
//        painter.drawLine(x0, y0 - tmpOffset, xGraph, y0 - tmpOffset);
//        tmpOffset+=stepY;
//    }
//    painter.end();
//}

//void MainWindow::drawGraphicParam()
//{
////    drawBackEnd();

////    int tmpType = getTypeByName(ui->comboBox->currentText());
////    if(!m_Data.contains(tmpType)) {
////         return;
////    }
////    QList<ParamGraph> tmpData;
////    double maxY;
////    int sizeX = 1100, sizeY = 580;
////    int x0 = 50, y0 = 500;
////    int xGraph = sizeX - x0;
////    int yGraph = sizeY - y0;
////    int sizeXgraph = xGraph - x0;
////    int sizeYgraph = y0 - yGraph;

////    maxY = 0;
////    for(int i = 0; i < m_Data[tmpType].size(); i++) {
////        if(!m_Chks[i]->isChecked()) {
////            continue;
////        }
////        ParamGraph tmpParam;
////        if(ui->comboBox_3->currentText() == "EV")
////            tmpParam.data = m_Data[tmpType][i]->m_dataComp[year][quarter]->EV;
////        else if(ui->comboBox_3->currentText() == "MCap")
////            tmpParam.data = m_Data[tmpType][i]->m_dataComp[year][quarter]->capitalization;
////        else if(ui->comboBox_3->currentText() == "EBITDA")
////            tmpParam.data = m_Data[tmpType][i]->m_dataComp[year][quarter]->EBITDA;
////        else if(ui->comboBox_3->currentText() == "PROFIT")
////            tmpParam.data = m_Data[tmpType][i]->m_dataComp[year][quarter]->cleanProfit/1000.;
////        else if(ui->comboBox_3->currentText() == "DEBT")
////            tmpParam.data = m_Data[tmpType][i]->m_dataComp[year][quarter]->debt/1000.;
////        else if(ui->comboBox_3->currentText() == "EV/PROFIT")
////            tmpParam.data = m_Data[tmpType][i]->m_dataComp[year][quarter]->ev_profit;
////        else if(ui->comboBox_3->currentText() == "P/E")
////            tmpParam.data = m_Data[tmpType][i]->m_dataComp[year][quarter]->p_e;

////        tmpParam.name = m_Data[tmpType][i]->m_name;
////        tmpData.append(tmpParam);
////        getMaxParam(maxY, tmpParam.data);
////    }

////    int countCompanys = tmpData.size();
////    int sizeRecX = sizeXgraph / (countCompanys*2);
////    int stepX = sizeRecX*2;
////    int sizeRecY = sizeYgraph - 20;

////    painter.begin(this);
////    painter.setRenderHint(QPainter::HighQualityAntialiasing);
////    QFont tmpFont;
////    tmpFont.setPixelSize(12);
////    painter.setFont(tmpFont);
////    painter.setPen(Qt::black);

////    //масштабы осьY
////    int stepY = sizeYgraph / 6;
////    int tmpOffset = stepY;
////    double stepMasht = maxY/6.;
////    double tmpMasht = stepMasht;
////    painter.drawText(x0 - 40, yGraph - 5, "млрд, руб");
////    for(int i = 1; i < 6; i++) {
////        QString tmpStr = QString::number(tmpMasht);
////        if(tmpStr.contains(".")) {
////            int tmpInt = tmpStr.indexOf(".");
////            tmpStr = tmpStr.left(tmpInt + 2);
////        }
////        painter.drawText(x0 - 40, y0 - tmpOffset + 2, tmpStr);
////        tmpOffset += stepY;
////        tmpMasht += stepMasht;
////    }

////    int tmpX = x0 + 10;
////    for(int i = 0; i < tmpData.size(); i++) {
////        int size = getSize(tmpData[i].data, sizeRecY, maxY);
////        painter.setBrush(Qt::darkRed);
////        painter.drawRect(tmpX, y0 - size, sizeRecX, size);
////        painter.drawText(tmpX + sizeRecX/2 - m_Data[tmpType][i]->m_name.size(), y0 + 20, tmpData[i].name);

////        tmpX+=stepX;
////    }

////    painter.end();
//}
//void MainWindow::paintEvent(QPaintEvent *event)
//{
////    if(ui->comboBox_2->currentText() == "FA")
////        drawGraphicFA();
////    else if(ui->comboBox_2->currentText() == "Param")
////        drawGraphicParam();
//}

