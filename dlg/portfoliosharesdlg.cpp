#include "portfoliosharesdlg.h"
#include "ui_portfoliosharesdlg.h"

portfolioSharesDlg::portfolioSharesDlg(QWidget *parent, CSql *sql) :
    QDialog(parent), m_Sql(sql),
    ui(new Ui::portfolioSharesDlg)
{
    ui->setupUi(this);

    m_curVolumeCompany = 0;

    m_purchaseDlg = new PurchaseDlg(this, sql);
    m_closePosDlg =  new PurchaseDlg(this, sql, typeDlgPurch::closePos);

    QStringList porfolios;
    porfolios<<"Oleg"<<"IIS"<<"Papa"<<"Mama"<<"Brat";

    m_TabWidget = new QTabWidget(this);
    m_TabWidget->resize(this->size().width(),this->size().height() - 50);

    QStringList horLabel;
    horLabel<<"Название"<<"Колич."<<"Средняя цена"<<"Тек. цена"<<"Рын. стоим."<<"P/L день %"<<"Чист. P/L %"<<"P/L";

    int i = 0;
    for(QString name : porfolios) {
        tableWidget_str tmpTabWidget;
        tmpTabWidget.tableWigdet = new QTableWidget(m_TabWidget);
        tmpTabWidget.tableWigdet->resize(this->size().width(),this->size().height() - 50);
        tmpTabWidget.tableWigdet->setColumnCount(8);
        tmpTabWidget.tableWigdet->setHorizontalHeaderLabels(horLabel);
        tmpTabWidget.tableWigdet->horizontalHeader()->setVisible(true);
        tmpTabWidget.tableWigdet->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        tmpTabWidget.tableWigdet->verticalHeader()->setVisible(false);
        tmpTabWidget.name = name;
        tmpTabWidget.index = i;
        connect(tmpTabWidget.tableWigdet, &QTableWidget::cellClicked, this, &portfolioSharesDlg::currentCompanyClicked);
        m_TabWidget->insertTab(i, tmpTabWidget.tableWigdet, name);
        m_tableWidgets.append(tmpTabWidget);
        i++;
    }

    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), SLOT(timerProc()));
    m_timer->start(100);
    m_timer->setInterval(10000);

    connect(m_purchaseDlg, &PurchaseDlg::portfolioSqlUpdate, this, &portfolioSharesDlg::updatePortfolioFromSql);
    connect(m_closePosDlg, &PurchaseDlg::portfolioSqlUpdate, this, &portfolioSharesDlg::updatePortfolioFromSql);
    connect(m_TabWidget, &QTabWidget::currentChanged, this, &portfolioSharesDlg::updatePortfolioFromSql);



    // Setup a signal mapper to avoid creating custom slots for each tab
    QSignalMapper *m = new QSignalMapper(this);

    for(int i = 0; i < 5; i++) {
        // Setup the shortcut for the first tab
        QShortcut *shortcut = new QShortcut(QKeySequence("F" + QString::number(i+1)), this);
        connect(shortcut, &QShortcut::activated, m, QOverload<>::of(&QSignalMapper::map));
        m->setMapping(shortcut, i);
    }


    // Wire the signal mapper to the tab widget index change slot
    connect(m, SIGNAL(mapped(int)), m_TabWidget, SLOT(setCurrentIndex(int)));

    updatePortfolioFromSql();
}

portfolioSharesDlg::~portfolioSharesDlg()
{
    delete ui;
}

void portfolioSharesDlg::setCurDayPrice(QList<DayPrice_str> curDayPrice, QString timeCurDP)
{
    m_curDayPrice = curDayPrice;
    m_timeCurDP = timeCurDP;
}

void portfolioSharesDlg::updatePortfolioFromSql()
{
    m_Purchases.clear();

    for(tableWidget_str tmpTableWidg : m_tableWidgets) {
        if(tmpTableWidg.index == m_TabWidget->currentIndex()) {
            m_Sql->readPortfolio(tmpTableWidg.name, m_Purchases);
            timerProc();
            return;
        }
    }

    qDebug()<<"ERR FIND NAME PORTFOLIO BY INDEX";
}

void portfolioSharesDlg::updateTabWidget()
{

}

void portfolioSharesDlg::timerProc()
{
    resTotal_str tmpResTotal;
    tmpResTotal.marketPrice = 0;
    tmpResTotal.buyPrice = 0;
    QList<resPorftolio_str> tmpResults;
    for(CompanyPurchase_str cmp : m_Purchases) {        
        for(DayPrice_str dayPrice: m_curDayPrice) {
            if(dayPrice.name == cmp.name) {
                resPorftolio_str tmpRes;
                tmpRes.name = cmp.name;
                tmpRes.volume = cmp.volume;
                tmpRes.priceShare = cmp.price;
                tmpRes.curPriceShare = dayPrice.price;
                tmpRes.marketPrice = dayPrice.price * cmp.volume;
                tmpRes.PLday_p = dayPrice.percentChange;
                tmpRes.percentChange = (tmpRes.curPriceShare - tmpRes.priceShare)/tmpRes.priceShare*100;
                tmpRes.PL = tmpRes.marketPrice - (tmpRes.priceShare * cmp.volume);;
                tmpResults.append(tmpRes);
                tmpResTotal.marketPrice += tmpRes.marketPrice;
                tmpResTotal.buyPrice += cmp.price * cmp.volume;
                break;
            }
        }
    }

    std::sort(tmpResults.begin(), tmpResults.end(), lessThan<resPorftolio_str>);

    resPorftolio_str tmpRes;
    tmpRes.name = "Total";
    tmpRes.marketPrice = tmpResTotal.marketPrice;
    tmpRes.PL = tmpResTotal.marketPrice - tmpResTotal.buyPrice;
    tmpResults.append(tmpRes);

    ui->label->setText(m_timeCurDP);

    for(tableWidget_str tmpTableWidg : m_tableWidgets) {
        if(tmpTableWidg.index == m_TabWidget->currentIndex()) {
            tmpTableWidg.tableWigdet->clearContents();
            tmpTableWidg.tableWigdet->setRowCount(tmpResults.size());

            int i = 0;
            for(resPorftolio_str res : tmpResults) {
                tmpTableWidg.tableWigdet->setItem(i, 0, new QTableWidgetItem(res.name));                
                tmpTableWidg.tableWigdet->setItem(i, 1, new QTableWidgetItem(QString::number(res.volume)));
                tmpTableWidg.tableWigdet->setItem(i, 2, new QTableWidgetItem(QString::number(res.priceShare)));
                tmpTableWidg.tableWigdet->setItem(i, 3, new QTableWidgetItem(QString::number(res.curPriceShare)));
                QString marketPrice;
                if(res.marketPrice >= 1000000)
                    marketPrice = QString::number(res.marketPrice/1000000., 'f', 2) + "M";
                else if(res.marketPrice >= 1000)
                    marketPrice = QString::number(res.marketPrice/1000., 'f', 2) + "K";
                tmpTableWidg.tableWigdet->setItem(i, 4, new QTableWidgetItem(marketPrice));
                tmpTableWidg.tableWigdet->setItem(i, 5, new QTableWidgetItem(QString::number(res.PLday_p, 'f',2)));
                tmpTableWidg.tableWigdet->setItem(i, 6, new QTableWidgetItem(QString::number(res.percentChange, 'f', 2)));
                tmpTableWidg.tableWigdet->setItem(i, 7, new QTableWidgetItem(QString::number(res.PL, 'f', 1)));
                for(int j = 0; j < 8; j++)
                    tmpTableWidg.tableWigdet->item(i, j)->setTextAlignment(Qt::AlignCenter);

                if(res.name == "Total") {
                    tmpTableWidg.tableWigdet->item(i,1)->setText("");
                    tmpTableWidg.tableWigdet->item(i,2)->setText("");
                    tmpTableWidg.tableWigdet->item(i,3)->setText("");
                    tmpTableWidg.tableWigdet->item(i,5)->setText("");
                    tmpTableWidg.tableWigdet->item(i,6)->setText("");
                }
                i++;
            }
            return;
        }
    }
}

void portfolioSharesDlg::currentCompanyClicked(int row)
{
    for(tableWidget_str tmpTableWidg : m_tableWidgets) {
        if(tmpTableWidg.index == m_TabWidget->currentIndex()) {
            m_curNameCompany = tmpTableWidg.tableWigdet->item(row,0)->text();
            m_curVolumeCompany = tmpTableWidg.tableWigdet->item(row,1)->text().toInt();
            return;
        }
    }
}
//добавить
void portfolioSharesDlg::on_pushButton_clicked()
{
    for(tableWidget_str tmpTableWidg : m_tableWidgets) {
        if(tmpTableWidg.index == m_TabWidget->currentIndex()) {
            m_purchaseDlg->setNamePorfolio(tmpTableWidg.name, m_curNameCompany);
            m_purchaseDlg->show();
            return;
        }
    }
}
//удалить
void portfolioSharesDlg::on_pushButton_2_clicked()
{
    for(tableWidget_str tmpTableWidg : m_tableWidgets) {
        if(tmpTableWidg.index == m_TabWidget->currentIndex()) {

            m_closePosDlg->setNamePorfolio(tmpTableWidg.name, m_curNameCompany, m_curVolumeCompany);
            m_closePosDlg->show();
            return;
        }
    }
}
