#ifndef PORTFOLIOSHARESDLG_H
#define PORTFOLIOSHARESDLG_H

#include <QDialog>
#include <QTabWidget>
#include <QTableWidget>
#include <QWidget>
#include <QHeaderView>
#include <QSignalMapper>
#include <QShortcut>
#include <QKeySequence>
#include <structs.h>
#include "purchasedlg.h"
#include "csql.h"


class resPorftolio_str
{
public:
    resPorftolio_str(){
        volume = 0;
        priceShare = 0;
        curPriceShare = 0;
        marketPrice = 0;
        PLday_p = 0;
        percentChange = 0;
        PL = 0;
    }

    QString name;
    int volume;
    double priceShare, curPriceShare;
    double marketPrice;
    double PLday_p;
    double percentChange;
    double PL;
};

struct resTotal_str
{
    double marketPrice, buyPrice;
    double percentChange;
};


struct tableWidget_str
{
    QTableWidget *tableWigdet;
    int index;
    QString name;
};

namespace Ui {
class portfolioSharesDlg;
}

class portfolioSharesDlg : public QDialog
{
    Q_OBJECT

public:
    explicit portfolioSharesDlg(QWidget *parent = nullptr, CSql *sql = nullptr);
    ~portfolioSharesDlg();

    QList<CompanyPurchase_str> m_Purchases;

    PurchaseDlg *m_purchaseDlg;
    PurchaseDlg *m_closePosDlg;

    QList<tableWidget_str> m_tableWidgets;

    QTabWidget *m_TabWidget;

    QList<DayPrice_str> m_curDayPrice;
    QString m_timeCurDP;

    CSql *m_Sql;

    QTimer *m_timer;

    void setCurDayPrice(QList<DayPrice_str> curDayPrice, QString timeCurDP);




public slots:
    void updatePortfolioFromSql();
    void updateTabWidget();
    void timerProc();
    void currentCompanyClicked(int row);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    QString m_curNameCompany;
    int m_curVolumeCompany;

    Ui::portfolioSharesDlg *ui;
};

#endif // PORTFOLIOSHARESDLG_H
