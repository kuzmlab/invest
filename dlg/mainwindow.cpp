#include "mainwindow.h"
#include "ui_mainwindow.h"

typeGraphDraw& operator++(typeGraphDraw &f)
{
    switch(f) {
        case typeGraphDraw::daily:      return f = typeGraphDraw::weekly;
        case typeGraphDraw::weekly:     return f = typeGraphDraw::monthly;
        case typeGraphDraw::monthly:    return f = typeGraphDraw::quaterly;
        case typeGraphDraw::quaterly:   return f = typeGraphDraw::yearly;
        case typeGraphDraw::yearly:     return f = typeGraphDraw::yearly;
    }
    return f;
}
typeGraphDraw& operator--(typeGraphDraw &f)
{
    switch(f) {
        case typeGraphDraw::daily:      return f = typeGraphDraw::daily;
        case typeGraphDraw::weekly:     return f = typeGraphDraw::daily;
        case typeGraphDraw::monthly:    return f = typeGraphDraw::weekly;
        case typeGraphDraw::quaterly:   return f = typeGraphDraw::monthly;
        case typeGraphDraw::yearly:     return f = typeGraphDraw::quaterly;
    }
    return f;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), m_threadCurPrice(nullptr), m_threadUpdateBD(nullptr)
{
    ui->setupUi(this);

    m_currentDateGraph = QDate::currentDate();

    QStringList tmpList;
    tmpList<<"ТРАНСПОРТ"<<"СЫРЬЕ"<<"АГРО"<<"ЭЛЕКТРО"<<"IT"<<"БАНКИ"<<"НЕФТЕГАЗ"<<"РИТЕЙЛ";
    ui->comboBox->addItems(tmpList);

    tmpList.clear();
    tmpList<<"Param"<<"FA";
    ui->comboBox_2->addItems(tmpList);

    tmpList.clear();
    tmpList<<"EV"<<"MCap"<<"EBITDA"<<"PROFIT"<<"DEBT"<<"EV/PROFIT"<<"P/E";
    ui->comboBox_3->addItems(tmpList);
    ui->comboBox_3->setVisible(true);

    connect(ui->comboBox, SIGNAL(currentTextChanged(QString)), SLOT(changeType(QString)));
    connect(ui->comboBox_2, SIGNAL(currentTextChanged(QString)), SLOT(changeTypeGraphic(QString)));
    connect(ui->comboBox_3, SIGNAL(currentTextChanged(QString)), SLOT(changeTypeParam(QString)));

   // m_Writer.readFinamLinks("finamListing.txt");
   // m_Writer.readData("data.xml");

    int xChk = 250, yChk = 600, stepChk = 30;
    for(int i = 0; i < 12; i++) {
        QCheckBox *tmpChk = new QCheckBox(this);
        connect(tmpChk, SIGNAL(clicked()), this, SLOT(update()));
        tmpChk->move(xChk, yChk);
        yChk += stepChk;
        if(i == 5) {
            xChk += 100;
            yChk = 600;
        }
        m_Chks.append(tmpChk);
    }

    updateCheckBoxes();

    m_DayDataDlg = new DayDataDlg(this, &m_Data);
   //m_DayDataDlg->show();
  //  m_DayDataDlg->move(1200,0);

    m_Sql = new CSql(this);
    m_Sql->readFinamLinks<CCompany>(&m_Data, "numFinam");
//    m_Sql->readFinamLinks<CCompany>(&m_Currencies, "currencyFinam");
//    m_Sql->readFinamLinks<CCompany>(&m_Futures, "futuresFinam");

    this->move(0,0);

    QRect rec = QApplication::desktop()->screenGeometry();
    //double height = rec.height();
    int width = rec.width();

    m_TodayPricesDlg = new todayPricesDlg(this, &m_Data);
    m_TodayPricesDlg->show();
    m_TodayPricesDlg->move(width - m_TodayPricesDlg->width() - 13, 0);
    connect(m_TodayPricesDlg, &todayPricesDlg::companyClicked, this, &MainWindow::drawGraphic);

    //init table
    QList<DayPrice_str> dayPrice;
    for(CCompany cmp : m_Data) {
        DayPrice_str tmpDP;
        tmpDP.percentChange = 0;
        tmpDP.name = cmp.getName();
        tmpDP.price = 0;
        dayPrice.append(tmpDP);
    }

    m_TodayPricesDlg->setCurDayPrice(dayPrice, m_curCurrenciesPrice, m_curFuturesPrice, "");

    QShortcut *shortcut = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_Left), this);
    connect(shortcut, &QShortcut::activated, this, &MainWindow::changeDateGraphicLeft);
    QShortcut *shortcut1 = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_Right), this);
    connect(shortcut1, &QShortcut::activated, this, &MainWindow::changeDateGraphicRight);
    QShortcut *shortcut2 = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_Up), this);
    connect(shortcut2, &QShortcut::activated, this, &MainWindow::changeDateGraphicUp);
    QShortcut *shortcut3 = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_Down), this);
    connect(shortcut3, &QShortcut::activated, this, &MainWindow::changeDateGraphicDown);
    QShortcut *shortcut4 = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_K), this);
    connect(shortcut4, &QShortcut::activated, this, &MainWindow::maximizeGraphic);

    m_AlgDayScalp = new CAlgDayScalp(this);

    on_pushButton_clicked();

    flagThread = 1;

    m_threadCurPrice = new std::thread(&MainWindow::getCurPrice, this);
    m_threadCurPrice->detach();

    m_threadUpdateBD = new std::thread(&MainWindow::updateBD, this);
    m_threadUpdateBD->detach();

    m_Sql->createTablePortfolio("Oleg");
    m_Sql->createTablePortfolio("IIS");
    m_Sql->createTablePortfolio("Papa");
    m_Sql->createTablePortfolio("Mama");
    m_Sql->createTablePortfolio("Brat");

    m_portfolioDlg = new portfolioSharesDlg(this, m_Sql);
    m_portfolioDlg->show();
    m_portfolioDlg->move(width - m_portfolioDlg->width() - 13, m_TodayPricesDlg->height() + 30);

    m_typeGraphDraw = typeGraphDraw::daily;
    nameGraphCmp = "";

    m_QuotesGraphicDlg = new QuotesGraphicDlg(this);
    m_QuotesGraphicDlg->init();
    m_QuotesGraphicDlg->show();
    connect(m_QuotesGraphicDlg, &QuotesGraphicDlg::wheelChange, this, &MainWindow::wheelEvent);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getMaxParam(double &maxParam, double curParam)
{
    if(maxParam < curParam)
        maxParam = curParam;
}

QString MainWindow::getTickerByName(const QString name)
{
    for(CCompany cmp : m_Data) {
        if(cmp.getName() == name)
            return cmp.getTicker();
    }
    return "";
}

void MainWindow::drawGraphic(QString name)
{
    int offsetDay = -60;//daily
    if(m_typeGraphDraw == typeGraphDraw::weekly)
        offsetDay *= 7;
    else if(m_typeGraphDraw == typeGraphDraw::monthly)
        offsetDay *= 30;
    else if(m_typeGraphDraw == typeGraphDraw::quaterly)
        offsetDay *= 90;
    else if(m_typeGraphDraw == typeGraphDraw::yearly)
        offsetDay *= 360;

    QDate dateFrom = m_currentDateGraph.addDays(offsetDay);
    QDate dateTo = m_currentDateGraph;

    QString ticker = getTickerByName(name);
    if(ticker == "") {
        qDebug()<<"error, dont find ticker by name: "<<name;
        return;
    }

    QList<str_priceDay> pricesCompany;
    if(m_typeGraphDraw == typeGraphDraw::daily)
        m_Sql->getDailyPriceHistoryByTicker(dateFrom, dateTo, ticker, pricesCompany);
    else if(m_typeGraphDraw == typeGraphDraw::weekly)
        m_Sql->getWeeklyPriceHistoryByTicker(dateFrom, dateTo, ticker, pricesCompany);
    else if(m_typeGraphDraw == typeGraphDraw::monthly)
        m_Sql->getMonthlyPriceHistoryByTicker(dateFrom, dateTo, ticker, pricesCompany);
    else if(m_typeGraphDraw == typeGraphDraw::quaterly)
        m_Sql->getQuaterlyPriceHistoryByTicker(dateFrom, dateTo, ticker, pricesCompany);
    else if(m_typeGraphDraw == typeGraphDraw::yearly)
        m_Sql->getYearlyPriceHistoryByTicker(dateFrom, dateTo, ticker, pricesCompany);

    m_QuotesGraphicDlg->draw(pricesCompany);
    nameGraphCmp = name;
}

void MainWindow::getCurPrice()
{
    QList<DayPrice_str> dayPrice;
    CDataGetter quotesGetter;
    while(flagThread) {
        dayPrice.clear();
        QString timeCurDP = QDateTime::currentDateTime().toString("hh:mm:ss");
        for(CCompany cmp : m_Data) {
            if(!flagThread) {
                flagThread = 5;
                qDebug()<<"finish Thread";
                return;
            }


            //FINAM
//            QDate currentDate = QDate::currentDate();
//            int offsetDay = -5;

//            QDate dateFrom = currentDate.addDays(offsetDay);
            //            QList<str_priceDay> tmpLP;
            //            quotesGetter->getData(cmp.getTicker(), cmp.getNumFinam(), dateFrom, currentDate, tmpLP);

            //            if(tmpLP.size() > 2) {
            //                double tmp = tmpLP.last().close - tmpLP[tmpLP.size()-2].close;
            //                DayPrice_str tmpDP;
            //                tmpDP.percentChange = tmp/tmpLP[tmpLP.size()-2].close*100;
            //                tmpDP.name = cmp.getName();
            //                tmpDP.price = tmpLP.last().close;
            //                dayPrice.append(tmpDP);
            //            }
            //INVESTING
            double price, changePercent;
            bool isPriceOk = quotesGetter.getInvesting(cmp.getLink(), price, changePercent);
            if(!isPriceOk)
                qDebug()<<cmp.getName()<<price<<changePercent;
            else {
                DayPrice_str tmpDP;
                tmpDP.percentChange = changePercent;
                tmpDP.name = cmp.getName();
                tmpDP.price = price;
                dayPrice.append(tmpDP);
            }
        }

        QList<DayPrice_str> curCurPrice;
//        for(CCompany cur : m_Currencies) {
//            if(!flagThread) {
//                flagThread = 5;//                
//                qDebug()<<"finish Thread";
//                return;
//            }

//            QDate currentDate = QDate::currentDate();
//            int offsetDay = -5;

//            QDate dateFrom = currentDate.addDays(offsetDay);

//            QList<str_priceDay> tmpLP;
//            quotesGetter->getData(cur.getTicker(), cur.getNumFinam(), dateFrom, currentDate, tmpLP);

//            if(tmpLP.size() > 2) {
//                double tmp = tmpLP.last().close - tmpLP[tmpLP.size()-2].close;
//                DayPrice_str tmpDP;
//                tmpDP.percentChange = tmp/tmpLP[tmpLP.size()-2].close*100;
//                tmpDP.name = cur.getName();
//                tmpDP.price = tmpLP.last().close;
//                curCurPrice.append(tmpDP);
//            }
//        }

        QList<DayPrice_str> curFutPrice;
//        for(CCompany cur : m_Futures) {
//            if(!flagThread) {
//                flagThread = 5;
//
//                return;
//            }

//            QDate currentDate = QDate::currentDate();
//            int offsetDay = -5;

//            QDate dateFrom = currentDate.addDays(offsetDay);

//            QList<str_priceDay> tmpLP;
//            quotesGetter->getData(cur.getTicker(), cur.getNumFinam(), dateFrom, currentDate, tmpLP);

//            if(tmpLP.size() > 2) {
//                double tmp = tmpLP.last().close - tmpLP[tmpLP.size()-2].close;
//                DayPrice_str tmpDP;
//                tmpDP.percentChange = tmp/tmpLP[tmpLP.size()-2].close*100;
//                tmpDP.name = cur.getName();
//                tmpDP.price = tmpLP.last().close;
//                curFutPrice.append(tmpDP);
//            }
//        }

        curPrice_mutex.lock();
        m_curDayPrice = dayPrice;
        m_curCurrenciesPrice = curCurPrice;
        m_curFuturesPrice = curFutPrice;
        m_TodayPricesDlg->setCurDayPrice(m_curDayPrice, m_curCurrenciesPrice, m_curFuturesPrice, timeCurDP);
        m_portfolioDlg->setCurDayPrice(m_curDayPrice, timeCurDP);
        curPrice_mutex.unlock();
    }
    flagThread = 5;    
}

void MainWindow::updateBD()
{
    CDataGetter quotesGetter;
    if(!m_Data.size()) {
        qDebug()<<"m_data.size==NULL"<<__FILE__<<__LINE__;
        return;
    }

    QDate currentDate = QDate::currentDate();
    int offsetDay = -15000;

    QDate dateFrom = currentDate.addDays(offsetDay);
    QDate dateTo = currentDate.addDays(-1);
    Sleep(1000);

    for(CCompany cmp : m_Data) {


        m_Sql->createTableCompany(cmp.getTicker());
        //корректируем dateFrom для базы, чтоб не делать пустые инсерты (dateFrom должен быть равен последней дате внесения цены в базу)
        if(!m_Sql->getLastDateByTicker(cmp.getTicker(), dateFrom)) {
            continue;
        }

        cmp.listPrice.clear();

        quotesGetter.getData(cmp.getTicker(), cmp.getNumFinam(), dateFrom, dateTo, cmp.listPrice);

        m_Sql->prepareQueryInsert(cmp.getTicker());

        for(str_priceDay &priceDay : cmp.listPrice) {
            m_Sql->insertDataPrice(priceDay);
        }
        Sleep(10);
    }

    qDebug()<<"finish update BD";
}

int MainWindow::getSize(double value, double size, double masht)
{
    double ret_val = value*size / masht;
    return (int)ret_val;
}

void MainWindow::changeType(QString type)
{
    updateCheckBoxes();

    update();
}

void MainWindow::changeTypeGraphic(QString type)
{
    if(ui->comboBox_2->currentText() == "Param")
        ui->comboBox_3->setVisible(true);
    else if(ui->comboBox_2->currentText() == "FA")
        ui->comboBox_3->setVisible(false);
    update();
}

void MainWindow::changeTypeParam(QString type)
{
    update();
}

void MainWindow::changeDateGraphicLeft()
{
    m_currentDateGraph = m_currentDateGraph.addDays(-1);
    on_pushButton_clicked();
}

void MainWindow::changeDateGraphicRight()
{
    m_currentDateGraph = m_currentDateGraph.addDays(1);
    on_pushButton_clicked();
}

void MainWindow::changeDateGraphicUp()
{

}

void MainWindow::changeDateGraphicDown()
{

}

void MainWindow::maximizeGraphic()
{
//    QSize availableSize = qApp->desktop()->availableGeometry().size();
//    this->resize(availableSize);
    //    plot->resize(availableSize.width(), availableSize.height()*0.9);
}

void MainWindow::wheelEvent(QWheelEvent *event)
{
    if(event->delta() > 0) {
        --m_typeGraphDraw;
    }
    if(event->delta() < 0) {
        ++m_typeGraphDraw;
    }
    drawGraphic(nameGraphCmp);
}

//добавить эмитента
void MainWindow::on_action_triggered()
{
    m_AddDataDlg.show();
}

//удалить эмитента
void MainWindow::on_action_2_triggered()
{
    m_DelDataDlg.show();
}


void MainWindow::updateCheckBoxes()
{
//    int tmpType = getTypeByName(ui->comboBox->currentText());
//    if(!m_Data.contains(tmpType)) {
//         return;
//    }
//    for(int i = 0; i < m_Chks.size(); i++) {
//        m_Chks[i]->setVisible(false);
//    }

//    for(int i = 0; i < m_Data[tmpType].size(); i++) {
//        if(i >= m_Chks.size())
//            break;

//        m_Chks[i]->setVisible(true);
//        m_Chks[i]->setChecked(true);
//        m_Chks[i]->setText(m_Data[tmpType][i]->m_name);
//        m_Chks[i]->setObjectName(m_Data[tmpType][i]->m_name);
//    }
}

void MainWindow::on_pushButton_clicked()
{

}

void MainWindow::on_pushButton_2_clicked()
{

}

void MainWindow::on_pushButton_3_clicked()
{
    QDate currentDate = QDate::currentDate();
    int offsetDay = -730;

    QDate dateFrom = currentDate.addDays(offsetDay);
    QDate dateTo = currentDate.addDays(-1);

    QList<str_priceDay> tmpDP;

    for(CCompany cmp : m_Data) {
        m_Sql->getDailyPriceHistoryByTicker(dateFrom, dateTo, cmp.getTicker(), tmpDP);
        m_AlgDayScalp->calc(cmp.getTicker(), tmpDP);
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    flagThread = 0;
    if(m_threadCurPrice != nullptr) {
        int maxCounter = 0;
        while(flagThread != 5) {
            if(maxCounter >= 50)
                break;
            Sleep(100);
            maxCounter++;
        }
    }

    qDebug()<<"close ok"<<flagThread;
    if(m_threadCurPrice != nullptr)
        delete m_threadCurPrice;
    if(m_threadUpdateBD != nullptr)
        delete m_threadUpdateBD;
}
