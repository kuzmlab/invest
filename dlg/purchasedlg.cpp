#include "purchasedlg.h"
#include "ui_purchasedlg.h"

PurchaseDlg::PurchaseDlg(QWidget *parent, CSql *sql, typeDlgPurch type) :
    QDialog(parent), m_Sql(sql),
    ui(new Ui::PurchaseDlg)
{
    ui->setupUi(this);

    typeDlg = type;

    if(typeDlg == typeDlgPurch::closePos) {
        ui->pushButton->setText("Закрыть");
    }
}

PurchaseDlg::~PurchaseDlg()
{
    delete ui;
}

void PurchaseDlg::setNamePorfolio(QString name, QString nameCmp, int volume)
{
    m_curNamePortfolio = name;
    m_curNameCmp = nameCmp;

    ui->lineEdit_name->setText(m_curNameCmp);
    ui->lineEdit_volume->setText(QString::number(volume));
    ui->lineEdit_date->setText(QDate::currentDate().toString("dd.MM.yyyy"));

}

void PurchaseDlg::on_pushButton_clicked()
{
    if(typeDlg == typeDlgPurch::purchasePos) {
        QString name = ui->lineEdit_name->text();
        int volume = ui->lineEdit_volume->text().toInt();
        double price = ui->lineEdit_price->text().toDouble();
        QString date = ui->lineEdit_date->text();
        int tmpVolume = 0;
        double tmpPrice = 0.0;
        //если уже покупали сегодня, апдейтим
        if(m_Sql->getVolumeCompIfBoughtToday(m_curNamePortfolio, name, date, tmpVolume, tmpPrice)) {
            double srPrice = (price*volume + tmpPrice*tmpVolume)/(price+tmpVolume);
            m_Sql->updateCmpPorftolioVolume(m_curNamePortfolio, name, tmpVolume, date);
            m_Sql->updateCmpPorftolioPrice(m_curNamePortfolio, name, srPrice, date);
        }
        else
            m_Sql->insertSharesInPortfolio(m_curNamePortfolio, name, volume, price, date);
    }
    else if(typeDlg == typeDlgPurch::closePos) {
        QString curNameCmp = ui->lineEdit_name->text();
        QList<CompanyPurchase_str> tmpData;
        m_Sql->getCompInPorftiolio(m_curNamePortfolio, curNameCmp, tmpData);
        int volume = ui->lineEdit_volume->text().toInt();
        double price = ui->lineEdit_price->text().toDouble();
        QString date = ui->lineEdit_date->text();
        int allVolume = 0;
        for (CompanyPurchase_str cmp : tmpData) {
            allVolume += cmp.volume;
        }
        //удаляем из портфеля
        if(volume >= allVolume) {
            m_Sql->delCmpPorftolio(m_curNamePortfolio, curNameCmp);
        }
        //начинаем разбор и апдейтим портфель согласно количеству (уменьшая)
        else {
            for (CompanyPurchase_str cmp : tmpData) {
                int tmpVolume = cmp.volume - volume;
                //уменьшаем количество акций
                if(tmpVolume > 0) {
                    m_Sql->updateCmpPorftolioVolume(m_curNamePortfolio, curNameCmp, tmpVolume, cmp.date);
                    break;
                }
                //удаляем запись
                else {
                    m_Sql->delCmpPorftolioByDate(m_curNamePortfolio, curNameCmp, cmp.date);
                    volume -= cmp.volume;
                }

                if(volume <= 0)
                    break;

            }
        }

    }
    emit portfolioSqlUpdate();
    this->close();
}
