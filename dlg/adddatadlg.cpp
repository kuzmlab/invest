#include "adddatadlg.h"
#include "ui_adddatadlg.h"
#include "defines.h"

AddDataDlg::AddDataDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDataDlg)
{
    ui->setupUi(this);

    QStringList tmpList;
    tmpList<<"СЫРЬЕ"<<"АГРО"<<"ЭЛЕКТРО"<<"ТРАНСПОРТ"<<"IT"<<"БАНКИ"<<"НЕФТЕГАЗ"<<"РИТЕЙЛ";
    ui->comboBox->addItems(tmpList);

    tmpList.clear();
    tmpList<<"долл, млн"<<"руб, млн";
    ui->comboBox_valuta->addItems(tmpList);

    tmpList.clear();
    tmpList<<"1"<<"2"<<"3"<<"4";
    ui->comboBox_Quarter->addItems(tmpList);

    tmpList.clear();
    tmpList<<"2016"<<"2017";
    ui->comboBox_Year->addItems(tmpList);

    connect(ui->lineEdit_Name, SIGNAL(textChanged(QString)), SLOT(change_lineedit_name(QString)));
    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), SLOT(changeCombo(int)));
    connect(ui->comboBox_Quarter, SIGNAL(currentIndexChanged(int)), SLOT(changeCombo(int)));
    connect(ui->comboBox_Year, SIGNAL(currentIndexChanged(int)), SLOT(changeCombo(int)));
}

AddDataDlg::~AddDataDlg()
{
    delete ui;
}

void AddDataDlg::on_pushButton_clicked()
{
    int typeValute = VAL_DOLLAR;
    if(ui->comboBox_valuta->currentText() == "долл, млн") {
        typeValute = VAL_DOLLAR;
    }
    else if(ui->comboBox_valuta->currentText() == "руб, млн") {
        typeValute = VAL_RUBLE;
    }

    int year = ui->comboBox_Year->currentText().toInt();
    int quarter = ui->comboBox_Quarter->currentText().toInt();

    CFinData *tmpFin = new CFinData(this);
    tmpFin->typeVal = typeValute;
    tmpFin->EV = ui->lineEdit_ev->text().toDouble();
    tmpFin->EBITDA = ui->lineEdit_ebitda->text().toDouble();
    tmpFin->debt = ui->lineEdit_debt->text().toDouble();
    tmpFin->capitalization = ui->lineEdit_capzation->text().toDouble();
    tmpFin->countShares = ui->lineEdit_countShares->text().toULongLong();
    tmpFin->countSharesPreffered = ui->lineEdit_countShares_Priv->text().toULongLong();
    tmpFin->freeFloat = ui->lineEdit_freefloat->text().toDouble();
    tmpFin->cash = ui->lineEdit_cash->text().toDouble();
    tmpFin->profitBeforeTaxes = ui->lineEdit_proftBe4Taxes->text().toDouble();
    tmpFin->percentPush       = ui->lineEdit_percentPush->text().toDouble();
    tmpFin->amortization      = ui->lineEdit_amortization->text().toDouble();
    tmpFin->cleanProfit       = ui->lineEdit_cleanProfit->text().toDouble();
    tmpFin->year      = year;
    tmpFin->quarter   = quarter;
    int tmpType = getTypeByName(ui->comboBox->currentText());
    QString tmpName = ui->lineEdit_Name->text();
    //ищем компанию в списке, если нету - создаем новую, если есть, туда пихаем финДату (либо обновляем)
//    for(int i = 0; i < m_Data[tmpType].size(); i++) {
//        if(m_Data[tmpType][i]->name == tmpName) {
//            //просто обновляем финдату
//            if(m_Data[tmpType][i]->m_dataComp.contains(year) && m_Data[tmpType][i]->m_dataComp[year].contains(quarter))
//                memcpy(m_Data[tmpType][i]->m_dataComp[year][quarter], tmpFin, sizeof(CFinData));
//            //добавляем в данные компании данный год и квартал
//            else
//                m_Data[tmpType][i]->m_dataComp[year].insert(quarter, tmpFin);
//            return;
//        }
//    }
    //создаем новую компанию и пихаем фин дату
//    CCompany *tmpComp = new CCompany();
//    tmpComp->m_name = tmpName;
//    tmpComp->m_ticker = ui->lineEdit_ticker->text();
//    tmpComp->m_tickerPreff = ui->lineEdit_ticker_Priv->text();
//    tmpComp->m_type = tmpType;
//    tmpComp->m_dataComp[year].insert(quarter, tmpFin);

    //m_Data[tmpType].append(tmpComp);
}

void AddDataDlg::change_lineedit_name(QString name)
{
//    int quarter = ui->comboBox_Quarter->currentText().toInt();
//    int year = ui->comboBox_Year->currentText().toInt();
//    int tmpType = getTypeByName(ui->comboBox->currentText());
//    QString ticker = ".ME";
//    QString tickerPref = ".ME";
//    for(int i = 0; i < m_Data[tmpType].size(); i++) {
//        if(m_Data[tmpType][i]->name == name) {
//            ticker = m_Data[tmpType][i]->ticker;
//            tickerPref = m_Data[tmpType][i]->tickerPreffered;
//            if(!m_Data[tmpType][i]->m_dataComp.contains(year))
//                break;
//            if(!m_Data[tmpType][i]->m_dataComp[year].contains(quarter))
//                break;

//            ui->lineEdit_ev->setText(QString::number(m_Data[tmpType][i]->m_dataComp[year][quarter]->EV));
//            ui->lineEdit_ebitda->setText(QString::number(m_Data[tmpType][i]->m_dataComp[year][quarter]->EBITDA));
//            ui->lineEdit_debt->setText(QString::number(m_Data[tmpType][i]->m_dataComp[year][quarter]->debt));
//            ui->lineEdit_capzation->setText(QString::number(m_Data[tmpType][i]->m_dataComp[year][quarter]->capitalization));
//            ui->lineEdit_countShares->setText(QString::number(m_Data[tmpType][i]->m_dataComp[year][quarter]->countShares));
//            ui->lineEdit_freefloat->setText(QString::number(m_Data[tmpType][i]->m_dataComp[year][quarter]->freeFloat));
//            ui->lineEdit_cash->setText(QString::number(m_Data[tmpType][i]->m_dataComp[year][quarter]->cash));
//            ui->lineEdit_proftBe4Taxes->setText(QString::number(m_Data[tmpType][i]->m_dataComp[year][quarter]->profitBeforeTaxes));
//            ui->lineEdit_percentPush->setText(QString::number(m_Data[tmpType][i]->m_dataComp[year][quarter]->percentPush));
//            ui->lineEdit_amortization->setText(QString::number(m_Data[tmpType][i]->m_dataComp[year][quarter]->amortization));
//            ui->lineEdit_cleanProfit->setText(QString::number(m_Data[tmpType][i]->m_dataComp[year][quarter]->cleanProfit));
//            ui->comboBox_valuta->setCurrentIndex(m_Data[tmpType][i]->m_dataComp[year][quarter]->typeVal);
//            return;
//        }
//    }

//    ui->lineEdit_ev->setText("");
//    ui->lineEdit_ebitda->setText("");
//    ui->lineEdit_debt->setText("");
//    ui->lineEdit_capzation->setText("");
//    ui->lineEdit_ticker->setText(ticker);
//    ui->lineEdit_ticker_Priv->setText(tickerPref);
//    ui->lineEdit_countShares->setText("");
//    ui->lineEdit_freefloat->setText("");
//    ui->lineEdit_cash->setText("");
//    ui->lineEdit_proftBe4Taxes->setText("");
//    ui->lineEdit_percentPush->setText("");
//    ui->lineEdit_amortization->setText("");
//    ui->lineEdit_cleanProfit->setText("");
//    ui->comboBox_valuta->setCurrentIndex(0);


}

void AddDataDlg::changeCombo(int tmp)
{
    change_lineedit_name(ui->lineEdit_Name->text());
}
