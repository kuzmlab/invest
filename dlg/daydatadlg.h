#ifndef DAYDATADLG_H
#define DAYDATADLG_H

#include <QDialog>
#include "defines.h"
#include "ccompany.h"
#include "cdatagetter.h"
#include "structs.h"




namespace Ui {
class DayDataDlg;
}

class DayDataDlg : public QDialog
{
    Q_OBJECT

public:
    explicit DayDataDlg(QWidget *parent = 0, QList<CCompany> *data = 0);
    ~DayDataDlg();

    CDataGetter getter;
    QList<CCompany> *m_Data;

    void getPriceMicex(CCompany &cmp);
private slots:
    void on_pushButton_clicked();

private:
    Ui::DayDataDlg *ui;
};

#endif // DAYDATADLG_H
