#ifndef ADDDATADLG_H
#define ADDDATADLG_H

#include <QDialog>

#include "ccompany.h"

namespace Ui {
class AddDataDlg;
}

class AddDataDlg : public QDialog
{
    Q_OBJECT

public:
    explicit AddDataDlg(QWidget *parent = 0);
    ~AddDataDlg();

private slots:
    void on_pushButton_clicked();

public slots:
    void change_lineedit_name(QString name);
    void changeCombo(int tmp);

private:
    Ui::AddDataDlg *ui;
};

#endif // ADDDATADLG_H
