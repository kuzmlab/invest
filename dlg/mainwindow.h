#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QCheckBox>
#include <QDebug>
#include <QtSql>
#include <QMouseEvent>

#include "qcustomplot.h"
#include "ccompany.h"
#include "adddatadlg.h"
#include "cdatawriter.h"
#include "deldatadlg.h"
#include "structs.h"
#include "cdatagetter.h"
#include "daydatadlg.h"
#include "csql.h"
#include "calgdayscalp.h"
#include "todaypricesdlg.h"
#include "portfoliosharesdlg.h"
#include "quotesgraphicdlg.h"
#include <thread>
#include <mutex>
#include "windows.h"


enum class typeGraphDraw {daily, weekly, monthly, quaterly, yearly};



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    AddDataDlg m_AddDataDlg;
    DelDataDlg m_DelDataDlg;
    DayDataDlg *m_DayDataDlg;
    todayPricesDlg *m_TodayPricesDlg;
    portfolioSharesDlg *m_portfolioDlg;
    QuotesGraphicDlg *m_QuotesGraphicDlg;

    typeGraphDraw m_typeGraphDraw;
    QString nameGraphCmp;

    CAlgDayScalp *m_AlgDayScalp;
    CDataWriter m_Writer;

    CSql *m_Sql;

    QList<CCompany> m_Data;
    QList<CCompany> m_Currencies;
    QList<CCompany> m_Futures;

    QDate m_currentDateGraph;

 //   void drawGraphicFA();
 //   void drawBackEnd();
  //  void drawGraphicParam();
    void updateCheckBoxes();
    int getSize(double value, double size, double masht);
    void getMaxParam(double &maxParam, double curParam);
    QString getTickerByName(const QString name);

    QPainter painter;

    QList<QCheckBox*> m_Chks;
    void getCurPrice();
    QList<DayPrice_str> m_curDayPrice;
    QList<DayPrice_str> m_curCurrenciesPrice;
    QList<DayPrice_str> m_curFuturesPrice;

    int flagThread = 0;

    void updateBD();


private:
    Ui::MainWindow *ui;
    std::thread *m_threadCurPrice;
    std::thread *m_threadUpdateBD;
    std::mutex curPrice_mutex;
public slots:
    void drawGraphic(QString name);
    void changeType(QString type);
    void changeTypeGraphic(QString type);
    void changeTypeParam(QString type);

    void changeDateGraphicLeft();
    void changeDateGraphicRight();
    void changeDateGraphicUp();
    void changeDateGraphicDown();
    void maximizeGraphic();

protected:
    void wheelEvent(QWheelEvent *event);
 //   void paintEvent(QPaintEvent *event);

private slots:
    void on_action_triggered();
    void on_action_2_triggered();
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
