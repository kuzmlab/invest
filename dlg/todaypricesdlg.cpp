#include "todaypricesdlg.h"
#include "ui_todaypricesdlg.h"

todayPricesDlg::todayPricesDlg(QWidget *parent, QList<CCompany> *data) :
    QDialog(parent), m_Data(data),
    ui(new Ui::todayPricesDlg)
{
    ui->setupUi(this);

    initTableWidget(ui->tableUp);
    initTableWidget(ui->tableDown);
    initTableWidget(ui->tableWidget_3);
    initTableWidget(ui->tableWidget_4);

    connect(ui->tableUp, &QTableWidget::cellClicked, this, &todayPricesDlg::cellClicked);
    connect(ui->tableDown, &QTableWidget::cellClicked, this, &todayPricesDlg::cellClicked);
}

todayPricesDlg::~todayPricesDlg()
{
    delete ui;
}

void todayPricesDlg::setCurDayPrice(QList<DayPrice_str> &curDayPrice, QList<DayPrice_str> &curCurPrice, QList<DayPrice_str> &curFuturesPrice, QString timeCurDP)
{
    m_curDayPrice = curDayPrice;
    m_curCurPrice = curCurPrice;
    m_curFuturesPrice = curFuturesPrice;
    m_timeCurDP = timeCurDP;
    updateDlg();
}

void todayPricesDlg::initTableWidget(QTableWidget *widget)
{
    QStringList horLabel;
    horLabel<<"ticker"<<"price"<<"change, %";
    widget->setColumnCount(3);
    widget->setHorizontalHeaderLabels(horLabel);
    widget->horizontalHeader()->setVisible(true);
    widget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    widget->verticalHeader()->setVisible(false);
}

void todayPricesDlg::updateTableWidget(QTableWidget *widget, const QList<DayPrice_str> &data)
{
    //TO DO разобраться куда деваются new QTableWidgetItem после setrowcount, если режем количество строк...
   // int countPrevRows = widget->rowCount();
    widget->setRowCount(data.size());

    int i = 0;
    for(DayPrice_str DP : data) {
        setItemTable(widget, i, 0, DP.name);
        setItemTable(widget, i, 1, QString::number(DP.price));
        setItemTable(widget, i, 2, QString::number(DP.percentChange,'f',1));
        i++;
    }
//    for(int j = i; j < countPrevRows; j++) {
//        setItemTable(widget, j, 0, "");
//        setItemTable(widget, j, 1, "");
//        setItemTable(widget, j, 2, "");
//    }
}

void todayPricesDlg::setItemTable(QTableWidget *widget, const int row, const int column, const QString data)
{
    if(!widget->item(row, column)) {
         widget->setItem(row, column, new QTableWidgetItem(data));
         widget->item(row, column)->setTextAlignment(Qt::AlignCenter);
    }
    else {
        widget->item(row, column)->setText(data);
    }
}

void todayPricesDlg::updateDlg()
{
    QList<DayPrice_str> DP_up, DP_down;
    for(DayPrice_str dp : m_curDayPrice) {
        if(dp.percentChange >= 0)
            DP_up.append(dp);
        else
            DP_down.append(dp);
    }

    std::sort(DP_up.begin(), DP_up.end(), lessThan<DayPrice_str>);
    std::sort(DP_down.begin(), DP_down.end(), greaterThan<DayPrice_str>);

    updateTableWidget(ui->tableUp, DP_up);
    updateTableWidget(ui->tableDown, DP_down);


//    ui->tableWidget_3->clearContents();
//    ui->tableWidget_3->setRowCount(m_curCurPrice.size() + m_curFuturesPrice.size());

//    i = 0;
//    for(DayPrice_str tmpDP : m_curCurPrice) {
//        ui->tableWidget_3->setItem(i, 0, new QTableWidgetItem(tmpDP.name));
//        ui->tableWidget_3->setItem(i, 1, new QTableWidgetItem(QString::number(tmpDP.price)));
//        ui->tableWidget_3->setItem(i, 2, new QTableWidgetItem(QString::number(tmpDP.percentChange,'f',2)));
//        for(int j = 0; j < 3; j++)
//            ui->tableWidget_3->item(i, j)->setTextAlignment(Qt::AlignCenter);
//        i++;
//    }

//    for(DayPrice_str tmpDP : m_curFuturesPrice) {
//        ui->tableWidget_3->setItem(i, 0, new QTableWidgetItem(tmpDP.name));
//        ui->tableWidget_3->setItem(i, 1, new QTableWidgetItem(QString::number(tmpDP.price)));
//        ui->tableWidget_3->setItem(i, 2, new QTableWidgetItem(QString::number(tmpDP.percentChange,'f',2)));
//        for(int j = 0; j < 3; j++)
//            ui->tableWidget_3->item(i, j)->setTextAlignment(Qt::AlignCenter);
//        i++;
//    }

    ui->label_3->setText(m_timeCurDP);
}

void todayPricesDlg::cellClicked(int row, int column)
{
    QTableWidget *tableWidget = qobject_cast<QTableWidget*>(sender());
    if(!column)
        emit companyClicked(tableWidget->item(row, column)->text());
}
