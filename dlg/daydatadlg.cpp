#include "daydatadlg.h"
#include "ui_daydatadlg.h"

DayDataDlg::DayDataDlg(QWidget *parent, QList<CCompany> *data) :
    QDialog(parent), m_Data(data),
    ui(new Ui::DayDataDlg)
{
    ui->setupUi(this);
}

DayDataDlg::~DayDataDlg()
{
    delete ui;
}

bool namefileGreaterThan(const WC_struct &d1, const WC_struct &d2)
{
    return d1.percentChange < d2.percentChange; // sort by namefile
}
bool namefileLessThan(const WC_struct &d1, const WC_struct &d2)
{
    return d1.percentChange > d2.percentChange; // sort by namefile
}

void DayDataDlg::on_pushButton_clicked()
{
//    QList<WC_struct> wc_long, wc_short;
//    DD_struct tmpDD;
//    tmpDD.countDays = ui->lineEdit_countDays->text().toInt();
//    tmpDD.percentThreshold = ui->lineEdit_percentThreshold->text().toDouble();
//    for(int i = 0; i < m_Data->size(); i++) {
//        getPriceMicex(m_Data->at(i));
//        WC_struct tmpWC;
//        m_Data->at(i)->analyzeDayData(tmpDD, tmpWC);
//        if(tmpWC.countDays) {
//            if(tmpWC.percentChange > 0)
//                wc_short.append(tmpWC);
//            else if(tmpWC.percentChange < 0)
//                wc_long.append(tmpWC);
//        }
//    }

//    qSort(wc_short.begin(), wc_short.end(), namefileLessThan);
//    qSort(wc_long.begin(), wc_long.end(), namefileGreaterThan);

//    QStringList horLabel;
//    horLabel<<"name"<<"countDays"<<"percentChange";
//    ui->tableWidget->setColumnCount(3);
//    ui->tableWidget->setHorizontalHeaderLabels(horLabel);
//    ui->tableWidget->horizontalHeader()->setVisible(true);
//    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
//    ui->tableWidget->verticalHeader()->setVisible(false);
//    ui->tableWidget->setRowCount(wc_short.size());
//    ui->tableWidget_2->setColumnCount(3);
//    ui->tableWidget_2->setHorizontalHeaderLabels(horLabel);
//    ui->tableWidget_2->horizontalHeader()->setVisible(true);
//    ui->tableWidget_2->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
//    ui->tableWidget_2->verticalHeader()->setVisible(false);
//    ui->tableWidget_2->setRowCount(wc_long.size());

//    for(int i = 0; i < wc_short.size(); i++) {
//        ui->tableWidget->setItem(i, 0, new QTableWidgetItem(wc_short[i].name));
//        ui->tableWidget->setItem(i, 1, new QTableWidgetItem(QString::number(wc_short[i].countDays)));
//        ui->tableWidget->setItem(i, 2, new QTableWidgetItem(QString::number(wc_short[i].percentChange,'g',2)));
//        for(int j = 0; j < 3; j++)
//            ui->tableWidget->item(i, j)->setTextAlignment(Qt::AlignCenter);
//    }
//    for(int i = 0; i < wc_long.size(); i++) {
//        ui->tableWidget_2->setItem(i, 0, new QTableWidgetItem(wc_long[i].name));
//        ui->tableWidget_2->setItem(i, 1, new QTableWidgetItem(QString::number(wc_long[i].countDays)));
//        ui->tableWidget_2->setItem(i, 2, new QTableWidgetItem(QString::number(wc_long[i].percentChange,'g',2)));
//        for(int j = 0; j < 3; j++)
//            ui->tableWidget_2->item(i, j)->setTextAlignment(Qt::AlignCenter);
//    }

}


void DayDataDlg::getPriceMicex(CCompany &cmp)
{
    cmp.listPrice.clear();
    QDate dayTo = QDate::currentDate();
    int offsetDay = -5;

    QDate dateFrom = dayTo.addDays(offsetDay);

    for(int i = 0; i < abs(offsetDay); i++) {
        QDate tmpDate = dateFrom.addDays(i);
        if(tmpDate.dayOfWeek() == 6 || tmpDate.dayOfWeek() == 7)
            offsetDay--;
    }

    dateFrom = dayTo.addDays(offsetDay);

    getter.getData(cmp.getTicker(), cmp.getNumFinam(), dateFrom, dayTo, cmp.listPrice);
}
