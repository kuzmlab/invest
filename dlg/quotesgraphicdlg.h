#ifndef QUOTESGRAPHICDLG_H
#define QUOTESGRAPHICDLG_H

#include <QWidget>
#include "qcustomplot.h"
#include "structs.h"

class QuotesGraphicDlg : public QWidget
{
    Q_OBJECT
public:
    explicit QuotesGraphicDlg(QWidget *parent = nullptr);

    void draw(const QList<str_priceDay> &pricesCompany);
    void init();

private:
    QCPFinancial *candlesticks;
    QCPBars *volumePos;
    QCPBars *volumeNeg;
    QCustomPlot *plot;


signals:
    void wheelChange(QWheelEvent *event);

public slots:
};

#endif // QUOTESGRAPHICDLG_H
