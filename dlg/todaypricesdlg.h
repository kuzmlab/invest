#ifndef TODAYPRICESDLG_H
#define TODAYPRICESDLG_H

#include <QDialog>
#include <QTimer>
#include <QTableWidget>

#include "defines.h"
#include "ccompany.h"
#include "structs.h"

namespace Ui {
class todayPricesDlg;
}

class todayPricesDlg : public QDialog
{
    Q_OBJECT

public:
    explicit todayPricesDlg(QWidget *parent = 0, QList<CCompany> *data = 0);
    ~todayPricesDlg();

    void setCurDayPrice(QList<DayPrice_str> &curDayPrice, QList<DayPrice_str> &curCurPrice, QList<DayPrice_str> &curFuturesPrice, QString timeCurDP);
    void updateDlg();

private:
    QList<CCompany> *m_Data;
    QList<DayPrice_str> m_curDayPrice;
    QList<DayPrice_str> m_curCurPrice;
    QList<DayPrice_str> m_curFuturesPrice;
    QString m_timeCurDP;

    void cellClicked(int row, int column);
    void initTableWidget(QTableWidget *widget);
    void updateTableWidget(QTableWidget *widget, const QList<DayPrice_str> &data);
    void setItemTable(QTableWidget *widget, const int row, const int column, const QString data);

    Ui::todayPricesDlg *ui;
signals:
    void companyClicked(QString name);
};

#endif // TODAYPRICESDLG_H
