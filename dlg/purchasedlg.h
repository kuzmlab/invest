#ifndef PURCHASEDLG_H
#define PURCHASEDLG_H

#include <QDialog>
#include "csql.h"
#include "structs.h"

namespace Ui {
class PurchaseDlg;
}

class PurchaseDlg : public QDialog
{
    Q_OBJECT

public:
    explicit PurchaseDlg(QWidget *parent = nullptr, CSql *sql = nullptr, typeDlgPurch type = typeDlgPurch::purchasePos);
    ~PurchaseDlg();

    void setNamePorfolio(QString name, QString nameCmp, int volume = 0);

private slots:
    void on_pushButton_clicked();

private:
    typeDlgPurch typeDlg;
    Ui::PurchaseDlg *ui;

    CSql *m_Sql;

    QString m_curNamePortfolio;
    QString m_curNameCmp;

signals:
    void portfolioSqlUpdate();
};

#endif // PURCHASEDLG_H
