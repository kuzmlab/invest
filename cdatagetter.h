#ifndef CDATAGETTER_H
#define CDATAGETTER_H

#include <QObject>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QUrl>
#include <QTextCodec>
#include <QTimer>
#include <QEventLoop>
#include <QXmlStreamReader>
#include "structs.h"
#include <thread>

class CDataGetter
{

public:
    CDataGetter();

    ~CDataGetter();



    void getData(QString ticker, int numFinam, QDate dateFrom, QDate dateTo, QList<str_priceDay> &data);

    bool getInvesting(const QString &link, double &price, double &changePercent);
private:
    bool sendRequest(QString requestStr, QList<str_priceDay> &data);
    bool sendReqInvesting(const QString &link, double &price, double &changePercent);
    QNetworkAccessManager manager;
    QNetworkReply *reply;

signals:

public slots:

};

#endif // CDATAGETTER_H
