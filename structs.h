#ifndef STRUCTS_H
#define STRUCTS_H

#include <QString>

enum class typeDlgPurch {purchasePos, closePos};

//class CCurrency
//{
//    CCurrency(QString name = "", QString ticker = "", int numFinam = 0, QString linkInvesting = ""
//     QString m_name, m_ticker, linkInvesting;
//     int m_numFinam;
//};

struct Futures
{
     QString name;
     int numFinam;
};

struct str_priceDay
{
    double high,low,open,close,volume;
    short year,month,day;
    QString date;
    qint64 dateSecs;
};

struct ParamGraph
{
    double data;
    QString name;
};

struct CompanyPurchase_str
{
    QString name, date, ticker;
    int volume;
    double price;
};

struct WC_struct
{
    QString name;
    int countDays;
    double percentChange;
};

struct DayPrice_str
{
    QString name;
    double percentChange;
    double price;
};

struct DD_struct
{
    double percentThreshold;
    int countDays;
};


#endif // STRUCTS_H
