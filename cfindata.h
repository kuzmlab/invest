#ifndef CFINDATA_H
#define CFINDATA_H

#include <QObject>
#include "defines.h"

class CFinData : public QObject
{
    Q_OBJECT
public:
    explicit CFinData(QObject *parent = 0);

    int typeVal;//тип валюты в входных данных (из отчета)
    double EV;
    double EBITDA;
    double debt;//dolg
    double equityMarket;//стоимость всех акций по рыночной стоимости
    double minorityInterest;//стоимость доли меньшинства ( по рыночной)
    double valueAssociate;//стоимость инвестиций в ассоциировапные компании
    double cash;//денежные средства
    double capitalization;
    double profitBeforeTaxes;//прибыль до налогообложения
    double percentPush;//проценты к уплате
    double amortization;//амортизация основных средств и нематериальных активов
    double cleanProfit;//чистая прибыль
    double prefferedInterest;//стоимость привелегированных акций (по рыночной)

    qulonglong countSharesPreffered;

    qulonglong countShares;

    double priceSharePreff;
    double priceShare;
    double freeFloat;
    double netDebt;//чистый долг

    int year;
    int quarter;

    double ev_profit;
    double p_e;
    double ev_ebitda;
    double debt_ebitda;

    void analyze();

signals:

public slots:

};

#endif // CFINDATA_H
