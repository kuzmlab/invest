#include "calgdayscalp.h"

#define STARTMONEY 100000
#define COUNTER_PARAM 5

CAlgDayScalp::CAlgDayScalp(QObject *parent) : QObject(parent)
{

}

void CAlgDayScalp::calc(QString ticker, QList<str_priceDay> dataPrices)
{
    double money = STARTMONEY;
    double prevMoney = 0;
    double prevClose = 0;
    int counterUp = 0, counterDown = COUNTER_PARAM;
    double countShares = 0;

    int countDeals = 0;
    int countPositiveDeals = 0;
    int maxCounterShares = 0;

    for(str_priceDay &priceDay : dataPrices) {
        if(!prevClose) {
            prevClose = priceDay.close;
            continue;
        }


        double deltaClose = priceDay.close - prevClose;
        double deltaPercent = deltaClose/prevClose*100;

        if(countShares > 0 && deltaPercent < 0.) {
            money = countShares * priceDay.close;
            countShares = 0;
            countDeals++;

            if(money > prevMoney) {
                countPositiveDeals++;
            }
        }
        else if(countShares < 0) {
//            money = countShares * priceDay.close + prevMoney;
//            countShares = 0;
//            countDeals++;

//            if(money > prevMoney) {
//                countPositiveDeals++;
//            }
        }


        if(deltaPercent < 0.) {
            counterDown = 0;
            counterUp++;
        }
        else {
            counterUp = 0;
            counterDown++;
        }


        if(counterUp >= COUNTER_PARAM) {
            //qDebug()<<ticker<<priceDay.date;
            countShares = money / priceDay.open;
            prevMoney = money;

            if(counterUp > maxCounterShares)
                maxCounterShares = counterUp;
        }
        if(counterDown >= COUNTER_PARAM) {
//            countShares = -money / priceDay.open;
//            prevMoney = money;
        }

        prevClose = priceDay.close;

    }
    if(countDeals)
        qDebug()<<ticker<<money<<countDeals<<QString::number((double)countPositiveDeals/(double)countDeals*100)+"%"<<QString::number((double)(countDeals-countPositiveDeals)/(double)countDeals*100)+"%"<<maxCounterShares;
   // if(money > STARTMONEY)

}
