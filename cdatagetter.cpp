#include "cdatagetter.h"

#include <QDesktopServices>
#include "windows.h"

CDataGetter::CDataGetter()
{

}

CDataGetter::~CDataGetter()
{

}

bool CDataGetter::sendRequest(QString requestStr, QList<str_priceDay> &data)
{
    QTimer timer;
    timer.setInterval(8000);
    timer.setSingleShot(true);

    QEventLoop loop;
    QUrl url(requestStr);
    QNetworkRequest request(url);
    reply = manager.get(request);
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    QObject::connect(&timer, &QTimer::timeout, reply, &QNetworkReply::abort);

    timer.start();
    loop.exec();

    if(reply->isFinished() && reply->error() == QNetworkReply::NoError) {
        QByteArray tmpDat;
        tmpDat = reply->readAll();
       // qDebug()<<tmpDat;
        if(tmpDat.isEmpty())
            return false;

        QList<QByteArray> tmpList = tmpDat.split('\n');
        for(int i = 1; i < tmpList.size()-1; i++) {
            QList<QByteArray> tmpRaw = tmpList[i].split(',');
            str_priceDay tmpDay;
            QString tmpDate(tmpRaw[2]);
            tmpDay.year = tmpDate.left(4).toShort();
            tmpDay.month = tmpDate.left(6).right(2).toShort();
            tmpDay.day = tmpDate.right(2).toShort();
            QString date = QString::number(tmpDay.day) + "." + QString::number(tmpDay.month) + "." + QString::number(tmpDay.year);
            tmpDay.date = date;
            tmpDay.open = tmpRaw[4].toDouble();
            tmpDay.high = tmpRaw[5].toDouble();
            tmpDay.low = tmpRaw[6].toDouble();
            tmpDay.close = tmpRaw[7].toDouble();
            tmpDay.volume = tmpRaw[8].left(tmpRaw[8].size() - 1).toDouble()/1000000;
            data.append(tmpDay);
        }
        reply->deleteLater();
        return true;
    }
    else {
        reply->deleteLater();
        return false;
        //_error = RequestSender::TimeoutError;
    }

  //  Sleep(2000);

}

bool CDataGetter::sendReqInvesting(const QString &link, double &price, double &changePercent)
{
    QTimer timer;
    timer.setInterval(5000);
    timer.setSingleShot(true);

    QEventLoop loop;
    QUrl url(link);
    QNetworkRequest request(url);

    QSslConfiguration sslConfiguration(QSslConfiguration::defaultConfiguration());
    request.setSslConfiguration(sslConfiguration);

    reply = manager.get(request);
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    QObject::connect(&timer, &QTimer::timeout, reply, &QNetworkReply::abort);

    timer.start();
    loop.exec();

    if(reply->isFinished() && reply->error() == QNetworkReply::NoError) {
        QByteArray tmpDat = reply->readAll();
        QString data(tmpDat);
        QString keyword_price = "lst";//ключевое слово на инвестинге для поиска цены в html

        int index = data.indexOf(keyword_price);

        QString tmpPrice, tmpPercentChange;
        bool bPrice = false, bPercentChange = false;
        int counterZn = 0;
        for(int i = 0; i < 500; i++) {
            if(bPercentChange && data[i + index] == "%")
                break;

            if(bPrice && data[i + index] == "<")
                bPrice = false;

            if(bPrice)
                tmpPrice += data[i + index];
            if(bPercentChange)
                tmpPercentChange += data[i + index];

            if(data[i + index] == ">") {
                if(!counterZn)
                    bPrice = true;

                counterZn++;
            }

            if(counterZn >= 5)
                bPercentChange = true;

        }
        //qDebug()<<tmpPrice<<link;
        tmpPrice.remove('.');
        tmpPrice.replace(',', '.');
        tmpPercentChange.replace(',', '.');

        double fPrice = tmpPrice.toDouble();
        double fPercentChange = tmpPercentChange.toDouble();

        if(!fPrice) {
            int stop;
            stop =0;
        }


        price = fPrice;
        changePercent = fPercentChange;

      //  qDebug()<<fPrice<<fPercentChange;

        reply->deleteLater();
        return true;
    }
    else {
        qDebug()<<reply->error();
        price = 0, changePercent = 0;
        reply->deleteLater();
        Sleep(5000);
        return false;
    }
}

void CDataGetter::getData(QString ticker, int numFinam, QDate dateFrom, QDate dateTo, QList<str_priceDay> &data)
{    
    int yearTo = dateTo.year();
    int monthTo = dateTo.month()-1;//for finam
    int dayTo = dateTo.day();

    int yearFrom = dateFrom.year();
    int monthFrom = dateFrom.month()-1;//for finam
    int dayFrom = dateFrom.day();

    QString str = "http://export.finam.ru/MGNT_170620_170623.txt?market=1";
    str += "&em="+ QString::number(numFinam);
    str += "&code="+ ticker;
    str += "&apply=0";//&df=2&mf=2&yf=2018&from=20.06.2017";";//&df=20&mf=5&yf=2017&from=20.06.2017";
    str += "&df=" + QString::number(dayFrom);
    str += "&mf=" + QString::number(monthFrom);
    str += "&yf=" + QString::number(yearFrom);
    str += "&from=" + QString::number(dayFrom) + "."+ QString::number(monthFrom) + "."+ QString::number(yearFrom);
    str += "&dt=" + QString::number(dayTo);
    str += "&mt=" + QString::number(monthTo);
    str += "&yt=" + QString::number(yearTo);
    str += "&to=" + QString::number(dayTo) + "."+ QString::number(monthTo) + "."+ QString::number(yearTo);
    str += "&p=8&f=MGNT_170620_170623&e=.txt&cn=MGNT&dtf=1&tmf=1&MSOR=1&mstime=on&mstimever=1&sep=1&sep2=1&datf=1&at=1";

   // qDebug()<<str;

//    QString str = "http://download.finance.yahoo.com/d/quotes.csv?";
//    str += "s=" + ticker;
//    str += "&f=l1";
//    str += "&ignore=.cs
    int counterTimeOut = 0;
    while(counterTimeOut < 10) {
        if(sendRequest(str, data))
            break;
        counterTimeOut++;
        Sleep(1000);

        //qDebug()<<ticker<<counterTimeOut;
    }
}

bool CDataGetter::getInvesting(const QString &link, double &price, double &changePercent)
{
    bool isGetPriceOk = false;
    QString str = "https://ru.investing.com/equities/" + link;

    int counterTimeOut = 0;
    while(counterTimeOut < 10) {
        if(sendReqInvesting(str, price, changePercent)) {
            isGetPriceOk = true;
            break;
        }
        counterTimeOut++;
        Sleep(1000);

        //qDebug()<<ticker<<counterTimeOut;
    }

    Sleep(1000);
    return isGetPriceOk;
}


//void CDataGetter::getDataByDate(QString ticker, double &data)
//{
//    mFlagReady = 0;
//    QString str = "http://download.finance.yahoo.com/d/quotes.csv?";
//    str += "s=" + ticker;
//    str += "&f=l1";
//    str += "&ignore=.csv";

//    sendRequest(str, data);
//}

