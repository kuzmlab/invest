#ifndef CCOMPANY_H
#define CCOMPANY_H

#include <QObject>
#include <QString>
#include "math.h"
#include "cdatagetter.h"
#include "defines.h"
#include "cfindata.h"
#include "structs.h"

struct Quarter
{
    int numQuarter;
    CFinData data;
};

class CCompany
{

public:
    explicit CCompany(QString name = "", QString ticker = "", int numFinam = 0, QString linkInvesting = "");

    QMap<int, QMap<int, CFinData*> > m_dataComp;

    QList<str_priceDay> listPrice;

    void analyzeDayData(DD_struct ddOpts, WC_struct &data);
    int getNumFinam();
    void setNumFinam(int numFinam);
    QString getTicker() {return m_ticker;}
    QString getName() {return m_name;}
    QString getLink() {return m_linkInvesting;}


private:

    QString m_name;
    QString m_ticker;
    int m_numFinam;
    QString m_linkInvesting;
    QString m_tickerPreff;
    int m_type;

};

#endif // CCOMPANY_H
