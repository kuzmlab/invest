#include "cdatawriter.h"


CDataWriter::CDataWriter(QObject *parent) :
    QObject(parent)
{
    stream.setAutoFormatting(true);
}

void CDataWriter::saveData(QString nameFile)
{
//    QFile file(nameFile);
//    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
//    if(!file.isOpen())
//        return;

//    stream.setDevice(&file);

//    stream.writeStartDocument();

//    QMapIterator<int, QList<CCompany*> > i(m_Data);
//    stream.writeStartElement("COMPANYS");
//    while (i.hasNext()) {
//        i.next();
//        QString nameOtrasl = getNameByType(i.key());
//        stream.writeStartElement(nameOtrasl);
//        for(int j = 0; j < i.value().size(); j++) {
//            QMapIterator<int, QMap<int, CFinData*> > k(i.value()[j]->m_dataComp);
//            while (k.hasNext()) {
//                k.next();
//                QMapIterator<int, CFinData*> z(k.value());
//                while (z.hasNext()) {
//                    z.next();
//                    stream.writeStartElement(i.value()[j]->name);
//                    stream.writeAttribute("type", QString::number(i.value()[j]->type));
//                    stream.writeAttribute("ticker", i.value()[j]->ticker);
//                    stream.writeAttribute("tickerPreffered", i.value()[j]->tickerPreffered);
//                    //stream.writeStartElement(QString::number(z.value()->year) + " Q" + QString::number(z.value()->quarter));
//                    stream.writeAttribute("EBITDA", QString::number(z.value()->EBITDA));
//                    stream.writeAttribute("EV", QString::number(z.value()->EV));
//                    stream.writeAttribute("debt", QString::number(z.value()->debt));
//                    stream.writeAttribute("capitalization", QString::number(z.value()->capitalization));
//                    stream.writeAttribute("countShares", QString::number(z.value()->countShares));
//                    stream.writeAttribute("countSharesPreffered", QString::number(z.value()->countSharesPreffered));
//                    stream.writeAttribute("freefloat", QString::number(z.value()->freeFloat));
//                    stream.writeAttribute("cash", QString::number(z.value()->cash));
//                    stream.writeAttribute("profitBeforeTaxes", QString::number(z.value()->profitBeforeTaxes));
//                    stream.writeAttribute("percentPush", QString::number(z.value()->percentPush));
//                    stream.writeAttribute("amortization", QString::number(z.value()->amortization));
//                    stream.writeAttribute("cleanProfit", QString::number(z.value()->cleanProfit));
//                    stream.writeAttribute("priceShare", QString::number(z.value()->priceShare));
//                    stream.writeAttribute("priceSharePreff", QString::number(z.value()->priceSharePreff));
//                    stream.writeAttribute("typeVal", QString::number(z.value()->typeVal));
//                    stream.writeAttribute("quarter", QString::number(z.value()->quarter));
//                    stream.writeAttribute("year", QString::number(z.value()->year));
//                    //stream.writeEndElement();//yearQuarter
//                    stream.writeEndElement();//name
//                }
//            }
//        }
//        stream.writeEndElement();//nameOtrasl
//    }

//    stream.writeStartElement("DOLLAR");
//    stream.writeAttribute("PRICE", QString::number(priceDollar));
//    stream.writeEndElement();

//    stream.writeEndElement();
//    stream.writeEndDocument();
//    file.close();
}

void CDataWriter::readData(QString nameFile)
{
//    QFile file(nameFile);
//    file.open(QIODevice::ReadOnly | QIODevice::Text);
//    if(!file.isOpen())
//        return;

//    reader.setDevice(&file);

//    while (!reader.atEnd()) {
//        QXmlStreamReader::TokenType token = reader.readNext();
//        if (token == QXmlStreamReader::StartDocument)
//            continue;
//        if (token == QXmlStreamReader::StartElement) {
//            QString tmpName = reader.name().toString();

//            if(tmpName == "DOLLAR") {
//                foreach(const QXmlStreamAttribute &attr, reader.attributes()) {
//                    if(attr.name().toString() == QLatin1String("PRICE"))
//                        priceDollar = attr.value().toString().toDouble();
//                }
//                continue;
//            }
//            if(tmpName == "RAW" || tmpName == "ELECTRO" || tmpName == "AGRO" || tmpName == "COMPANYS" ||
//               tmpName == "TRANSPORT" || tmpName == "IT" || tmpName == "BANK" || tmpName == "OILGAZ" || tmpName == "RETAIL")
//                continue;

//            CFinData *tmpFin = new CFinData(this);
//            QString ticker, tickerPreffered;
//            int type;
//            foreach(const QXmlStreamAttribute &attr, reader.attributes()) {
//                if(attr.name().toString() == QLatin1String("ticker"))
//                    ticker = attr.value().toString();
//                if(attr.name().toString() == QLatin1String("tickerPreffered"))
//                    tickerPreffered = attr.value().toString();
//                if(attr.name().toString() == QLatin1String("type"))
//                    type = attr.value().toString().toInt();
//                if(attr.name().toString() == QLatin1String("EBITDA"))
//                    tmpFin->EBITDA = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("EV"))
//                    tmpFin->EV = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("debt"))
//                    tmpFin->debt = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("countShares"))
//                    tmpFin->countShares = attr.value().toString().toULongLong();
//                if(attr.name().toString() == QLatin1String("countSharesPreffered"))
//                    tmpFin->countSharesPreffered = attr.value().toString().toULongLong();
//                if(attr.name().toString() == QLatin1String("capitalization"))
//                    tmpFin->capitalization = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("freefloat"))
//                    tmpFin->freeFloat = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("cash"))
//                    tmpFin->cash = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("profitBeforeTaxes"))
//                    tmpFin->profitBeforeTaxes = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("percentPush"))
//                    tmpFin->percentPush = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("amortization"))
//                    tmpFin->amortization = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("cleanProfit"))
//                    tmpFin->cleanProfit = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("priceShare"))
//                    tmpFin->priceShare = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("priceSharePreff"))
//                    tmpFin->priceSharePreff = attr.value().toString().toDouble();
//                if(attr.name().toString() == QLatin1String("typeVal"))
//                    tmpFin->typeVal = attr.value().toString().toInt();
//                if(attr.name().toString() == QLatin1String("quarter"))
//                    tmpFin->quarter = attr.value().toString().toInt();
//                if(attr.name().toString() == QLatin1String("year")) {
//                    tmpFin->year = attr.value().toString().toInt();
//                }
//            }

//            int flagFind = 0;
//            //ищем компанию в списке, если нету - создаем новую
//            for(int i = 0; i < m_Data[type].size(); i++) {
//                if(m_Data[type][i]->name == tmpName) {
//                    m_Data[type][i]->m_dataComp[tmpFin->year].insert(tmpFin->quarter, tmpFin);
//                    flagFind = 1;
//                    break;
//                }
//            }

//            if(!flagFind) {
//                CCompany *tmpComp = new CCompany(this);
//                tmpComp->ticker = ticker;
//                tmpComp->tickerPreffered = tickerPreffered;
//                tmpComp->type = type;
//                tmpComp->name = tmpName;
//                tmpComp->m_dataComp[tmpFin->year].insert(tmpFin->quarter, tmpFin);
//                m_Data[tmpComp->type].append(tmpComp);
//            }

//        }
//    }
//    file.close();
}

void CDataWriter::readFinamLinks(QString nameFile)
{
//    QFile file(nameFile);
//    file.open(QIODevice::ReadOnly | QIODevice::Text);
//    if(!file.isOpen()) {
//        qDebug()<<"No finam links!!";
//        return;
//    }

//    QTextStream in(&file);
//    while (!in.atEnd()) {
//        QString line = in.readLine();
//        CCompany *tmpComp = new CCompany(this);
//        QStringList tmp = line.split(",");
//        tmpComp->name = tmp[0];
//        tmpComp->setNumFinam(tmp[3].toInt());
//        tmpComp->ticker = tmp[4];
//        m_Data[TYPE_ALL].append(tmpComp);
//    }
}
