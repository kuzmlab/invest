#include "defines.h"

int getTypeByName(QString name)
{
    int tmpType = 0;
    if(name == "СЫРЬЕ")
        tmpType = TYPE_RAW;
    if(name == "АГРО")
        tmpType = TYPE_AGRO;
    if(name == "ЭЛЕКТРО")
        tmpType = TYPE_ELEKTRO;
    if(name == "ТРАНСПОРТ")
        tmpType = TYPE_TRANSPORT;
    if(name == "IT")
        tmpType = TYPE_IT;
    if(name == "БАНКИ")
        tmpType = TYPE_BANK;
    if(name == "НЕФТЕГАЗ")
        tmpType = TYPE_OILGAS;
    if(name == "РИТЕЙЛ")
        tmpType = TYPE_RETAIL;

    return tmpType;
}

QString getNameByType(int type)
{
    QString tmp = "ERR";
    if(type == TYPE_RAW)
        tmp = "RAW";
    if(type == TYPE_AGRO)
        tmp = "AGRO";
    if(type == TYPE_ELEKTRO)
        tmp = "ELECTRO";
    if(type == TYPE_TRANSPORT)
        tmp = "TRANSPORT";
    if(type == TYPE_IT)
        tmp = "IT";
    if(type == TYPE_BANK)
        tmp = "BANK";
    if(type == TYPE_OILGAS)
        tmp = "OILGAZ";
    if(type == TYPE_RETAIL)
        tmp = "RETAIL";

    return tmp;
}


